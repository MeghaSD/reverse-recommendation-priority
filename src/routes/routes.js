const express = require('express');
const mongoose = require('mongoose');
var router = express.Router();
const authController = require('../controllers/authController');
var validator = require('../middleware/validator');
var userController = require('../controllers/userController');
var priorityController =  require('../controllers/priorityController')

/*
    testing api
*/
router.get('/demoTesting', (req, res) => {
    res.json({
        status: 200,
        message: "demo testing router success"
    })
});


/*
    Use this api for login user  (Date: 13-05-2022)
*/
router.post('/login', authController.login);

// Protect all routes after this middleware (Date: 26-02-2022)
// router.use(authController.protect);

/*
    Save Client Informations (Date: 13-05-2022)
*/
router.post('/saveWebsites', validator.emailExist,userController.saveWebsites);

/*
    Save Fulfillment Priority Rules (Date: 13-05-2022)
*/
router.post('/savePriorityRules', priorityController.savePriorityRules)

/*
    Get Fulfillment Priority Rules Using tenant_id and rule_code (Date: 13-05-2022)
*/
router.post('/getPriorityRules', priorityController.getPriorityRules)

/*
    Search client name  (Date: 13-05-2022)
*/
router.post('/searchClientName', priorityController.searchClientName)

/*
    validation Fulfillment Priority Rules api With Existing Rule Validation (Date: 13-05-2022)
*/
router.post('/validationPriorityRules', priorityController.validationPriorityRules)

/*
    Delete recommendations rules api using tenant_id and rule_code (Date: 13-05-2022)
*/
router.delete('/removePriorityRules', priorityController.removePriorityRules)

/*
    Get route by dynamic mysql database connection (Date: 14-05-2022)
*/
router.post('/getCourierData', priorityController.getCourierData)

/*
    Get recommendation priority rule api (Date: 14-05-2022)
*/
router.post('/getRecommendationPriority', priorityController.getZones, priorityController.getRecommendationPriority)

module.exports = router;