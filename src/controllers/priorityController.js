const priorityRulesModel = require('../models/priority_rules');
var trim = require('trim');
var systemDao = require('../dao/system_dao');
const websites2Model = require('../models/websites2');
var dbClient = require('../config/db.client');
const ilogix_standard_pincodes_master = require("../dao/ilogix_standard_pincode_dao");
const consignor_standard_billing_zones = require("../dao/consignor_standard_zones_dao");
const ilogix_standard_other_pincodes = require("../dao/ilogix_other_pincode_dao");
var commanController = require('./commanController');

exports.savePriorityRules = async (req, res, next) => {
    if (req.body.tenant_id == undefined) {
        res.json({
            message: 'Required all fields....'
        })
    } else {
        if (req.body.platform && (req.body.platform !== undefined || req.body.platform !== 'undefined' || req.body.platform !== '')) {
            var platform = trim(req.body.platform)
            if (req.body.pay_type != undefined) {
                if (req.body.rule_code == 'WZ') {
                    if (req.body.weight == undefined || req.body.zone == undefined || req.body.list == undefined) {
                        res.json({
                            message: 'Validation: Required zone, weight and priority...'
                        })
                    } else {
                        var tenant_id = trim(req.body.tenant_id)
                        var pay_type = trim(req.body.pay_type)
                        var rule_code = trim(req.body.rule_code)
                        var zone = trim(req.body.zone)
                        var weight = trim(req.body.weight)
                        var arr = weight.split("-")
                        var from = trim(arr[0])
                        var to = trim(arr[1])
                        // console.log("from----",from)
                        // console.log("to----",to)

                        var newlist = [];

                        req.body.list.forEach(function (data) {
                            if (data.courier_code) {
                                newlist.push({
                                    courier_code: data.courier_code,
                                })
                            }
                        })


                        var existsData = []
                        if (req.body._id) {
                            var _id = trim(req.body._id)
                            existsData = await priorityRulesModel.find({
                                _id: _id
                            })
                            if (existsData.length != 0) {
                                try {
                                    priorityRulesModel.findOneAndUpdate(
                                        {
                                            _id: _id                                       //check this related data in collection
                                        },
                                        {
                                            $set: {
                                                tenant_id: tenant_id,
                                                rule_code: rule_code,
                                                weight: weight,
                                                pay_type: pay_type,
                                                zone: zone,
                                                from: from,
                                                to: to,                                   // update this are column value
                                                list: newlist
                                            }
                                        },
                                        { upsert: true, new: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one.
                                        function (err, priorityRules) {
                                            if (err) {
                                                // console.log(err);
                                                res.json({
                                                    status: 400,
                                                    message: 'Some Error Occured During Creation.'
                                                })
                                            } else {
                                                res.json({
                                                    status: 200,
                                                    data: priorityRules._id,
                                                    message: 'save successfully'
                                                });
                                            }
                                        });

                                } catch (error) {
                                    // console.log(`Error: ${error.message}`)
                                    return res.status(500).json({
                                        error: error.message
                                    })
                                }
                            }
                        } else {
                            var newlist = [];
                            req.body.list.forEach(function (data) {
                                if (data.courier_code) {
                                    newlist.push({
                                        courier_code: data.courier_code,
                                    })
                                }
                            })
                            try {
                                var priorityData = new priorityRulesModel({
                                    tenant_id: tenant_id,
                                    rule_code: rule_code,
                                    weight: weight,
                                    pay_type: pay_type,
                                    zone: zone,
                                    from: from,
                                    to: to,                                   // update this are column value
                                    list: newlist
                                });
                                priorityData.save(function (err, priorityRules) {
                                    if (err) {
                                        res.json({
                                            status: 400,
                                            message: 'Some Error Occured During Creation.'
                                        })
                                    } else {
                                        res.json({
                                            status: 200,
                                            data: priorityRules._id,
                                            message: 'save successfully'
                                        });
                                    }
                                });
                            } catch (error) {
                                // console.log(`Error: ${error.message}`)
                                return res.status(500).json({
                                    error: error.message
                                })
                            }
                        }

                    }
                } else if (req.body.rule_code == 'ZM') {
                    if (req.body.zone == undefined || req.body.list == undefined) {
                        res.json({
                            message: 'Validation: Required zone and priority...'
                        })
                    } else {
                        var tenant_id = trim(req.body.tenant_id)
                        var pay_type = trim(req.body.pay_type)
                        var rule_code = trim(req.body.rule_code)
                        var zone = trim(req.body.zone)
                        var newlist = [];
                        req.body.list.forEach(function (data) {
                            if (data.courier_code) {
                                newlist.push({
                                    courier_code: data.courier_code,
                                })
                            }
                        })
                        var existsData = []
                        if (req.body._id) {
                            var _id = trim(req.body._id)
                            existsData = await priorityRulesModel.find({
                                _id: _id
                            })
                            if (existsData.length != 0) {
                                try {
                                    priorityRulesModel.findOneAndUpdate(
                                        {
                                            _id: _id              //check this related data in collection
                                        },
                                        {
                                            $set: {
                                                tenant_id: tenant_id,
                                                rule_code: rule_code,
                                                weight: 'null',
                                                pay_type: pay_type,
                                                zone: zone,
                                                from: 'null',
                                                to: 'null', // update this are column value                      
                                                list: newlist
                                            }
                                        },
                                        { upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                                        function (err, priorityRules) {
                                            if (err) {
                                                // console.log(err);
                                                res.json({
                                                    status: 400,
                                                    message: 'Some Error Occured During Creation.'
                                                })
                                            } else {
                                                // console.log(priorityRules);
                                                res.json({
                                                    status: 200,
                                                    data: priorityRules._id,
                                                    message: 'save successfully'
                                                });
                                            }
                                        });

                                } catch (error) {
                                    // console.log(`Error: ${error.message}`)
                                    return res.status(500).json({
                                        error: error.message
                                    })
                                }
                            }
                        } else {
                            var newlist = [];
                            req.body.list.forEach(function (data) {
                                if (data.courier_code) {
                                    newlist.push({
                                        courier_code: data.courier_code,
                                    })
                                }
                            })
                            try {
                                var priorityData = new priorityRulesModel({
                                    tenant_id: tenant_id,
                                    rule_code: rule_code,
                                    pay_type: pay_type,
                                    zone: zone,                         // update this are column value
                                    list: newlist
                                });
                                priorityData.save(function (err, priorityRules) {
                                    if (err) {
                                        res.json({
                                            status: 400,
                                            message: 'Some Error Occured During Creation.'
                                        })
                                    } else {
                                        res.json({
                                            status: 200,
                                            data: priorityRules._id,
                                            message: 'save successfully'
                                        });
                                    }
                                });
                            } catch (error) {
                                // console.log(`Error: ${error.message}`)
                                return res.status(500).json({
                                    error: error.message
                                })
                            }
                        }

                    }
                } else if (req.body.rule_code == 'WM') {
                    if (req.body.weight == undefined || req.body.list == undefined) {
                        res.json({
                            message: 'Validation: Required weight and priority...'
                        })
                    } else {
                        var tenant_id = trim(req.body.tenant_id)
                        var pay_type = trim(req.body.pay_type)
                        var rule_code = trim(req.body.rule_code)
                        var weight = trim(req.body.weight)
                        var arr = weight.split("-");
                        var from = trim(arr[0])
                        var to = trim(arr[1])
                        // console.log("from----",from)
                        // console.log("to----",to)
                        var newlist = [];
                        req.body.list.forEach(function (data) {
                            if (data.courier_code) {
                                newlist.push({
                                    courier_code: data.courier_code,
                                })
                            }
                        })
                        var existsData = []
                        if (req.body._id) {
                            var _id = trim(req.body._id)
                            existsData = await priorityRulesModel.find({
                                _id: _id
                            })
                            if (existsData.length != 0) {
                                try {
                                    priorityRulesModel.findOneAndUpdate(
                                        {
                                            _id: _id                //check this related data in collection
                                        },
                                        {
                                            $set: {
                                                tenant_id: tenant_id,
                                                rule_code: rule_code,
                                                weight: weight,
                                                zone: 'null',
                                                pay_type: pay_type,
                                                from: from,
                                                to: to,                                   // update this are column value                      
                                                list: newlist
                                            }
                                        },
                                        { upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                                        function (err, priorityRules) {
                                            if (err) {
                                                // console.log(err);
                                                res.json({
                                                    status: 400,
                                                    message: 'Some Error Occured During Creation.'
                                                })
                                            } else {
                                                // console.log(priorityRules);
                                                res.json({
                                                    status: 200,
                                                    data: priorityRules._id,
                                                    message: 'save successfully'
                                                });
                                            }
                                        });

                                } catch (error) {
                                    // console.log(`Error: ${error.message}`)
                                    return res.status(500).json({
                                        error: error.message
                                    })
                                }
                            }
                        } else {
                            var newlist = [];
                            req.body.list.forEach(function (data) {
                                if (data.courier_code) {
                                    newlist.push({
                                        courier_code: data.courier_code,
                                    })
                                }
                            })
                            try {
                                var priorityData = new priorityRulesModel({
                                    tenant_id: tenant_id,
                                    rule_code: rule_code,
                                    weight: weight,
                                    pay_type: pay_type,
                                    from: from,
                                    to: to,                        // update this are column value
                                    list: newlist
                                });
                                priorityData.save(function (err, priorityRules) {
                                    if (err) {
                                        res.json({
                                            status: 400,
                                            message: 'Some Error Occured During Creation.'
                                        })
                                    } else {
                                        res.json({
                                            status: 200,
                                            data: priorityRules._id,
                                            message: 'save successfully'
                                        });
                                    }
                                });
                            } catch (error) {
                                // console.log(`Error: ${error.message}`)
                                return res.status(500).json({
                                    error: error.message
                                })
                            }
                        }

                    }
                } else if (req.body.list) {
                    var tenant_id = trim(req.body.tenant_id)
                    var pay_type = trim(req.body.pay_type)
                    var newlist = [];
                    console.log("req.body.list---", req.body.list)
                    req.body.list.forEach(function (data) {
                        if (data.courier_code) {
                            newlist.push({
                                courier_code: data.courier_code,
                            })
                        }
                    })

                    var existsData = []
                    if (req.body._id) {
                        var _id = trim(req.body._id)
                        // existsData = await priorityRulesModel.find({
                        //     _id: _id
                        // })
                        // if (existsData.length != 0) {
                        try {
                            priorityRulesModel.findOneAndUpdate(
                                {
                                    _id: _id
                                },
                                {
                                    $set: {
                                        tenant_id: tenant_id,
                                        pay_type: pay_type,
                                        rule_code: 'default_rule',
                                        weight: 'null',
                                        zone: 'null',
                                        from: 'null',
                                        to: 'null',
                                        list: newlist                           // update this are column value
                                    }
                                },
                                { upsert: true, new: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                                async function (err, priorityRules) {
                                    if (err) {
                                        // console.log(err);
                                        res.json({
                                            status: 400,
                                            message: 'Some Error Occured During Creation.'
                                        })
                                    } else {
                                        // res.json({
                                        //   status: 200,
                                        //   message: 'save successfully'
                                        // });
                                        if (platform.toUpperCase() == 'V1') {
                                            if (req.body.database && (req.body.database !== undefined || req.body.database !== 'undefined' || req.body.database !== '')) {
                                                var database = trim(req.body.database)
                                                var list = req.body.list
                                                var newlist = [];
                                                req.body.list.forEach(function (data) {
                                                    if (data.courier_code) {
                                                        newlist.push({
                                                            courier_code: data.courier_code,
                                                        })
                                                    }
                                                })
                                                var courier_code = []
                                                var count = 0
                                                newlist.forEach(async function (element) {
                                                    count++
                                                    await courier_code.push({
                                                        courier: element.courier_code,
                                                        priority: count
                                                    })

                                                })
                                                var list = [];
                                                var courier_vendor_list = [];
                                                var courier_master_list = [];
                                                var courier_vendors = await systemDao.reverseCourier()   // check on system db using courier_vendors table
                                                dbClient.getMysqlDBClient(database).then(async function (connection2) {
                                                    var sql = "select * from courier_master where is_active=1 AND priority IS NOT NULL order by priority asc";
                                                    connection2.query(sql, (error, results) => {
                                                        if (error) {
                                                            return res.status(400).json({
                                                                message: 'Bad Request',
                                                            })
                                                        } else {
                                                            console.log("results----", results.length)
                                                            if (courier_vendors != '') {
                                                                for (let data of courier_vendors) {
                                                                    // console.log("courier_vendors---",data.api_code)
                                                                    courier_vendor_list.push({
                                                                        vendor: data.code
                                                                    })
                                                                }
                                                            } else {
                                                                courier_vendor_list = []
                                                            }

                                                            if (results != '') {
                                                                for (let result of results) {
                                                                    // console.log("results---",result.api_code)
                                                                    courier_master_list.push({
                                                                        master: result.courier_code
                                                                    })
                                                                }
                                                            } else {
                                                                courier_master_list = []
                                                            }

                                                            // console.log("courier_vendor_list--",courier_vendor_list)
                                                            // console.log("courier_master_list--",courier_master_list)
                                                            if (courier_vendor_list.length > 0 && courier_master_list.length > 0) {
                                                                for (var i = 0; i <= courier_vendor_list.length - 1; i++) {
                                                                    console.log("courier_vendor_list@--", courier_vendor_list[i].vendor,)
                                                                    for (var j = 0; j < courier_master_list.length; j++) {
                                                                        if (courier_vendor_list[i] && (courier_vendor_list[i].vendor === courier_master_list[j].master)) {
                                                                            console.log("@@@-----", courier_vendor_list[i].vendor, courier_master_list[j].master)
                                                                            list.push({
                                                                                courier_code: courier_vendor_list[i].vendor
                                                                            })
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                list = []
                                                            }
                                                            console.log("list--", list)
                                                            if (courier_code.length != 0) {
                                                                courier_code.forEach(async function (element1) {
                                                                    var courier = element1.courier
                                                                    var priority = element1.priority
                                                                    if (courier) {

                                                                        let el = await list.find(itm => itm.courier_code === courier);

                                                                        if (el) {
                                                                            console.log("el---", el)
                                                                            console.log("courier--", courier)
                                                                            console.log("priority----", priority)
                                                                            var sql2 = "UPDATE courier_master SET priority = " + priority + ", is_active = " + 1 + " WHERE courier_code = '" + courier + "' AND is_active = 1";
                                                                            connection2.query(sql2, (error, results) => {
                                                                                if (error) {
                                                                                    return res.status(400).json({
                                                                                        message: 'Bad Request',
                                                                                    })
                                                                                } else {
                                                                                    // console.log("results----",results)                              
                                                                                }
                                                                            })
                                                                        }
                                                                    }
                                                                    let el3 = await list.some(item => item.courier_code === courier)
                                                                    console.log("el3----", el3)
                                                                    if (el3 == false) {
                                                                        console.log("courier@----", courier)
                                                                        console.log("priority@----", priority)
                                                                        var sql3 = "UPDATE courier_master SET priority = " + priority + ", is_active = " + 1 + " WHERE courier_code = '" + courier + "' ";
                                                                        connection2.query(sql3, (error, results) => {
                                                                            if (error) {
                                                                                return res.status(400).json({
                                                                                    message: 'Bad Request',
                                                                                })
                                                                            } else {
                                                                                // console.log("results----",results)                              
                                                                            }
                                                                        })
                                                                    }
                                                                })
                                                            }

                                                            if (list.length != 0) {
                                                                list.forEach(async function (data) {
                                                                    var resultCourier = data.courier_code
                                                                    let el2 = await courier_code.some(item => item.courier === resultCourier)
                                                                    // console.log("el2---",el2)                             
                                                                    if (el2 == false) {
                                                                        var sql2 = "UPDATE courier_master SET is_active = " + 0 + " WHERE courier_code = '" + resultCourier + "' AND is_active = 1";
                                                                        // console.log("sql2---",sql2)
                                                                        connection2.query(sql2, (error, results) => {
                                                                            if (error) {
                                                                                return res.status(400).json({
                                                                                    message: 'Bad Request',
                                                                                })
                                                                            } else {
                                                                                // console.log("results----",results)                              
                                                                            }
                                                                        })
                                                                    } else if (el2 == true) {

                                                                    }
                                                                })
                                                            }



                                                        }
                                                    })
                                                })
                                            } else {
                                                res.json({
                                                    message: 'Database is required....'
                                                })
                                            }
                                        } else if (platform.toUpperCase() == 'V2') {
                                            var newlist = [];
                                            req.body.list.forEach(function (data) {
                                                if (data.courier_code) {
                                                    newlist.push({
                                                        courier_code: data.courier_code,
                                                    })
                                                }
                                            })
                                            let result = newlist.map(a => a.courier_code);
                                            console.log("result---", result)
                                            websiteData = await websites2Model.find({
                                                _id: tenant_id
                                            })
                                            var list = [];
                                            var finalList = [];
                                            if (websiteData != '') {
                                                for (let data of websiteData) {
                                                    console.log("data----", data['reverse_enabled_couriers'])
                                                    for (let element of data['reverse_enabled_couriers']) {
                                                        var isIncluded = result.includes(element);
                                                        if (isIncluded == false) {
                                                            console.log("isIncluded--", isIncluded)
                                                            console.log("element----", element)
                                                            list.push(element)
                                                        } else {
                                                            list = []
                                                        }

                                                    }
                                                    console.log("data----", data['reverse_couriers_history'])
                                                    if (data['reverse_couriers_history']) {
                                                        finalList = data['reverse_couriers_history'].concat(list)
                                                    } else {
                                                        finalList = list
                                                    }
                                                }
                                            } else {
                                                finalList = []
                                            }
                                            console.log("finalList----", finalList)
                                            try {
                                                websites2Model.findOneAndUpdate(
                                                    {
                                                        _id: tenant_id
                                                    },
                                                    {
                                                        $set: {
                                                            reverse_enabled_couriers: result,
                                                            reverse_couriers_history: finalList                       // update this are column value
                                                        }
                                                    },
                                                    { upsert: true, new: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                                                    function (err, priorityRules) {
                                                        if (err) {
                                                            // console.log(err);
                                                            res.json({
                                                                status: 400,
                                                                message: 'Some Error Occured During Creation.'
                                                            })
                                                        } else {
                                                            // console.log(priorityRules);
                                                            // res.json({
                                                            //   status: 200,
                                                            //   message: 'save successfully'
                                                            // });
                                                        }
                                                    });
                                            } catch (error) {
                                                // console.log(`Error: ${error.message}`)
                                                return res.status(500).json({
                                                    error: error.message
                                                })
                                            }
                                        }
                                        res.json({
                                            status: 200,
                                            data: priorityRules._id,
                                            message: 'save successfully'
                                        });
                                    }

                                });

                        } catch (error) {
                            // console.log(`Error: ${error.message}`)
                            return res.status(500).json({
                                error: error.message
                            })
                        }
                        // }
                    } else {
                        var newlist = [];
                        req.body.list.forEach(function (data) {
                            if (data.courier_code) {
                                newlist.push({
                                    courier_code: data.courier_code,
                                })
                            }
                        })
                        try {
                            var priorityData = new priorityRulesModel({
                                tenant_id: tenant_id,
                                pay_type: pay_type,
                                rule_code: 'default_rule',
                                list: newlist
                            });
                            priorityData.save(async function (err, priorityRules) {
                                if (err) {
                                    res.json({
                                        status: 400,
                                        message: 'Some Error Occured During Creation.'
                                    })
                                } else {
                                    // res.json({
                                    //   status: 200,
                                    //   message: 'save successfully'
                                    // });
                                    if (platform.toUpperCase() == 'V1') {
                                        if (req.body.database && (req.body.database !== undefined || req.body.database !== 'undefined' || req.body.database !== '')) {
                                            var database = trim(req.body.database)
                                            var list = req.body.list
                                            var newlist = [];
                                            req.body.list.forEach(function (data) {
                                                if (data.courier_code) {
                                                    newlist.push({
                                                        courier_code: data.courier_code,
                                                    })
                                                }
                                            })
                                            var courier_code = []
                                            var count = 0
                                            newlist.forEach(async function (element) {
                                                count++
                                                await courier_code.push({
                                                    courier: element.courier_code,
                                                    priority: count
                                                })

                                            })
                                            var list = [];
                                            var courier_vendor_list = [];
                                            var courier_master_list = [];
                                            var courier_vendors = await systemDao.reverseCourier()   // check on system db using courier_vendors table
                                            dbClient.getMysqlDBClient(database).then(async function (connection2) {
                                                var sql = "select * from courier_master where is_active=1 AND priority IS NOT NULL order by priority asc";
                                                connection2.query(sql, (error, results) => {
                                                    if (error) {
                                                        return res.status(400).json({
                                                            message: 'Bad Request',
                                                        })
                                                    } else {
                                                        // console.log("results----",results)
                                                        if (courier_vendors != '') {
                                                            for (let data of courier_vendors) {
                                                                // console.log("courier_vendors---",data.api_code)
                                                                courier_vendor_list.push({
                                                                    vendor: data.code
                                                                })
                                                            }
                                                        } else {
                                                            courier_vendor_list = []
                                                        }

                                                        if (results != '') {
                                                            for (let result of results) {
                                                                // console.log("results---",result.api_code)
                                                                courier_master_list.push({
                                                                    master: result.courier_code
                                                                })
                                                            }
                                                        } else {
                                                            courier_master_list = []
                                                        }

                                                        // console.log("courier_vendor_list--",courier_vendor_list)
                                                        // console.log("courier_master_list--",courier_master_list)
                                                        if (courier_vendor_list.length > 0 && courier_master_list.length > 0) {
                                                            for (var i = 0; i <= courier_vendor_list.length - 1; i++) {
                                                                console.log("courier_vendor_list@--", courier_vendor_list[i].vendor,)
                                                                for (var j = 0; j < courier_master_list.length; j++) {
                                                                    if (courier_vendor_list[i] && (courier_vendor_list[i].vendor === courier_master_list[j].master)) {
                                                                        console.log("@@@-----", courier_vendor_list[i].vendor, courier_master_list[j].master)
                                                                        list.push({
                                                                            courier_code: courier_vendor_list[i].vendor
                                                                        })
                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            list = []
                                                        }
                                                        if (courier_code.length != 0) {
                                                            courier_code.forEach(async function (element1) {
                                                                var courier = element1.courier
                                                                var priority = element1.priority
                                                                if (courier) {
                                                                    let el = await list.find(itm => itm.courier_code === courier);
                                                                    if (el) {
                                                                        var sql2 = "UPDATE courier_master SET priority = " + priority + ", is_active = " + 1 + " WHERE courier_code = '" + courier + "' AND is_active = 1";
                                                                        connection2.query(sql2, (error, results) => {
                                                                            if (error) {
                                                                                return res.status(400).json({
                                                                                    message: 'Bad Request',
                                                                                })
                                                                            } else {
                                                                                // console.log("results----",results)                              
                                                                            }
                                                                        })
                                                                    }
                                                                }
                                                                let el3 = await list.some(item => item.courier_code === courier)
                                                                // console.log("el3----",el3)
                                                                if (el3 == false) {
                                                                    // console.log("courier----",courier)
                                                                    var sql3 = "UPDATE courier_master SET priority = " + priority + ", is_active = " + 1 + " WHERE courier_code = '" + courier + "' ";
                                                                    connection2.query(sql3, (error, results) => {
                                                                        if (error) {
                                                                            return res.status(400).json({
                                                                                message: 'Bad Request',
                                                                            })
                                                                        } else {
                                                                            // console.log("results----",results)                              
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }

                                                        if (list.length != 0) {
                                                            list.forEach(async function (data) {
                                                                var resultCourier = data.courier_code
                                                                let el2 = await courier_code.some(item => item.courier === resultCourier)
                                                                // console.log("el2---",el2)                             
                                                                if (el2 == false) {
                                                                    var sql2 = "UPDATE courier_master SET is_active = " + 0 + " WHERE courier_code = '" + resultCourier + "' AND is_active = 1";
                                                                    // console.log("sql2---",sql2)
                                                                    connection2.query(sql2, (error, results) => {
                                                                        if (error) {
                                                                            return res.status(400).json({
                                                                                message: 'Bad Request',
                                                                            })
                                                                        } else {
                                                                            // console.log("results----",results)                              
                                                                        }
                                                                    })
                                                                } else if (el2 == true) {

                                                                }
                                                            })
                                                        }



                                                    }
                                                })
                                            })
                                        } else {
                                            res.json({
                                                message: 'Database is required....'
                                            })
                                        }
                                    } else if (platform.toUpperCase() == 'V2') {
                                        var newlist = [];
                                        req.body.list.forEach(function (data) {
                                            if (data.courier_code) {
                                                newlist.push({
                                                    courier_code: data.courier_code,
                                                })
                                            }
                                        })
                                        let result = newlist.map(a => a.courier_code);
                                        console.log("result---", result)
                                        websiteData = await websites2Model.find({
                                            _id: tenant_id
                                        })
                                        var list = [];
                                        var finalList = [];
                                        if (websiteData != '') {
                                            for (let data of websiteData) {
                                                console.log("data----", data['reverse_enabled_couriers'])
                                                for (let element of data['reverse_enabled_couriers']) {
                                                    var isIncluded = result.includes(element);
                                                    if (isIncluded == false) {
                                                        console.log("isIncluded--", isIncluded)
                                                        console.log("element----", element)
                                                        list.push(element)
                                                    } else {
                                                        list = []
                                                    }

                                                }
                                                console.log("data----", data['reverse_couriers_history'])
                                                if (data['reverse_couriers_history']) {
                                                    finalList = data['reverse_couriers_history'].concat(list)
                                                } else {
                                                    finalList = list
                                                }
                                            }
                                        } else {
                                            finalList = []
                                        }
                                        console.log("finalList----", finalList)
                                        try {
                                            websites2Model.findOneAndUpdate(
                                                {
                                                    _id: tenant_id
                                                },
                                                {
                                                    $set: {
                                                        reverse_enabled_couriers: result,                          // update this are column value
                                                        reverse_couriers_history: finalList
                                                    }
                                                },
                                                { upsert: true, new: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                                                function (err, priorityRules) {
                                                    if (err) {
                                                        // console.log(err);
                                                        res.json({
                                                            status: 400,
                                                            message: 'Some Error Occured During Creation.'
                                                        })
                                                    } else {
                                                        // console.log(priorityRules);
                                                        // res.json({
                                                        //   status: 200,
                                                        //   message: 'save successfully'
                                                        // });
                                                    }
                                                });
                                        } catch (error) {
                                            // console.log(`Error: ${error.message}`)
                                            return res.status(500).json({
                                                error: error.message
                                            })
                                        }
                                    }
                                    res.json({
                                        status: 200,
                                        data: priorityRules._id,
                                        message: 'save successfully'
                                    });
                                }

                            });
                        } catch (error) {
                            // console.log(`Error: ${error.message}`)
                            return res.status(500).json({
                                error: error.message
                            })
                        }
                    }
                } else {
                    res.json({
                        message: 'Required all fields....'
                    })
                }
            } else if (req.body.pay_type == undefined) {
                if (req.body.rule_code == 'WZ') {
                    if (req.body.weight == undefined || req.body.zone == undefined || req.body.list == undefined) {
                        res.json({
                            message: 'Validation: Required zone, weight and priority...'
                        })
                    } else {
                        var tenant_id = trim(req.body.tenant_id)
                        var rule_code = trim(req.body.rule_code)
                        var zone = trim(req.body.zone)
                        var weight = trim(req.body.weight)
                        var arr = weight.split("-");
                        var from = trim(arr[0])
                        var to = trim(arr[1])
                        // console.log("from----",from)
                        // console.log("to----",to)
                        var newlist = [];
                        req.body.list.forEach(function (data) {
                            if (data.courier_code) {
                                newlist.push({
                                    courier_code: data.courier_code,
                                })
                            }
                        })
                        var existsData = []
                        if (req.body._id) {
                            var _id = trim(req.body._id)
                            existsData = await priorityRulesModel.find({
                                _id: _id
                            })
                            if (existsData.length != 0) {
                                try {
                                    priorityRulesModel.findOneAndUpdate(
                                        {
                                            _id: _id            //check this related data in collection
                                        },
                                        {
                                            $set: {
                                                tenant_id: tenant_id,
                                                rule_code: rule_code,
                                                weight: weight,
                                                pay_type: 'BOTH',
                                                zone: zone,
                                                from: from,
                                                to: to,                                // update this are column value                     
                                                list: newlist
                                            }
                                        },
                                        { upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                                        function (err, priorityRules) {
                                            if (err) {
                                                // console.log(err);
                                                res.json({
                                                    status: 400,
                                                    message: 'Some Error Occured During Creation.'
                                                })
                                            } else {
                                                // console.log(priorityRules);
                                                res.json({
                                                    status: 200,
                                                    data: priorityRules._id,
                                                    message: 'save successfully'
                                                });
                                            }
                                        });

                                } catch (error) {
                                    // console.log(`Error: ${error.message}`)
                                    return res.status(500).json({
                                        error: error.message
                                    })
                                }
                            }
                        } else {
                            var newlist = [];
                            req.body.list.forEach(function (data) {
                                if (data.courier_code) {
                                    newlist.push({
                                        courier_code: data.courier_code,
                                    })
                                }
                            })
                            try {
                                var priorityData = new priorityRulesModel({
                                    tenant_id: tenant_id,
                                    rule_code: rule_code,
                                    weight: weight,
                                    pay_type: 'BOTH',
                                    zone: zone,
                                    from: from,
                                    to: to,                                // update this are column value                     
                                    list: newlist
                                });
                                priorityData.save(function (err, priorityRules) {
                                    if (err) {
                                        res.json({
                                            status: 400,
                                            message: 'Some Error Occured During Creation.'
                                        })
                                    } else {
                                        res.json({
                                            status: 200,
                                            data: priorityRules._id,
                                            message: 'save successfully'
                                        });
                                    }
                                });
                            } catch (error) {
                                // console.log(`Error: ${error.message}`)
                                return res.status(500).json({
                                    error: error.message
                                })
                            }
                        }

                    }
                } else if (req.body.rule_code == 'ZM') {
                    if (req.body.zone == undefined || req.body.list == undefined) {
                        res.json({
                            message: 'Validation: Required zone and priority...'
                        })
                    } else {
                        var tenant_id = trim(req.body.tenant_id)
                        var rule_code = trim(req.body.rule_code)
                        var zone = trim(req.body.zone)
                        var newlist = [];
                        req.body.list.forEach(function (data) {
                            if (data.courier_code) {
                                newlist.push({
                                    courier_code: data.courier_code,
                                })
                            }
                        })
                        var existsData = []
                        if (req.body._id) {
                            var _id = trim(req.body._id)
                            existsData = await priorityRulesModel.find({
                                _id: _id
                            })
                            if (existsData.length != 0) {
                                try {
                                    priorityRulesModel.findOneAndUpdate(
                                        {
                                            _id: _id                    //check this related data in collection
                                        },
                                        {
                                            $set: {
                                                tenant_id: tenant_id,
                                                rule_code: rule_code,
                                                weight: 'null',
                                                pay_type: 'BOTH',
                                                zone: zone,
                                                from: 'null',
                                                to: 'null',                                // update this are column value                     
                                                list: newlist
                                            }
                                        },
                                        { upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                                        function (err, priorityRules) {
                                            if (err) {
                                                // console.log(err);
                                                res.json({
                                                    status: 400,
                                                    message: 'Some Error Occured During Creation.'
                                                })
                                            } else {
                                                // console.log(priorityRules);
                                                res.json({
                                                    status: 200,
                                                    data: priorityRules._id,
                                                    message: 'save successfully'
                                                });
                                            }
                                        });

                                } catch (error) {
                                    // console.log(`Error: ${error.message}`)
                                    return res.status(500).json({
                                        error: error.message
                                    })
                                }
                            }
                        } else {
                            var newlist = [];
                            req.body.list.forEach(function (data) {
                                if (data.courier_code) {
                                    newlist.push({
                                        courier_code: data.courier_code,
                                    })
                                }
                            })
                            try {
                                var priorityData = new priorityRulesModel({
                                    tenant_id: tenant_id,
                                    rule_code: rule_code,
                                    pay_type: 'BOTH',
                                    zone: zone,                                  // update this are column value                     
                                    list: newlist
                                });
                                priorityData.save(function (err, priorityRules) {
                                    if (err) {
                                        res.json({
                                            status: 400,
                                            message: 'Some Error Occured During Creation.'
                                        })
                                    } else {
                                        res.json({
                                            status: 200,
                                            data: priorityRules._id,
                                            message: 'save successfully'
                                        });
                                    }
                                });
                            } catch (error) {
                                // console.log(`Error: ${error.message}`)
                                return res.status(500).json({
                                    error: error.message
                                })
                            }
                        }

                    }
                } else if (req.body.rule_code == 'WM') {
                    if (req.body.weight == undefined || req.body.list == undefined) {
                        res.json({
                            message: 'Validation: Required weight and priority...'
                        })
                    } else {
                        var tenant_id = trim(req.body.tenant_id)
                        var rule_code = trim(req.body.rule_code)
                        var weight = trim(req.body.weight)
                        var arr = weight.split("-");
                        var from = trim(arr[0])
                        var to = trim(arr[1])
                        // console.log("from----",from)
                        // console.log("to----",to)
                        var newlist = [];
                        req.body.list.forEach(function (data) {
                            if (data.courier_code) {
                                newlist.push({
                                    courier_code: data.courier_code,
                                })
                            }
                        })
                        var existsData = []
                        if (req.body._id) {
                            var _id = trim(req.body._id)
                            existsData = await priorityRulesModel.find({
                                _id: _id
                            })
                            if (existsData.length != 0) {
                                try {
                                    priorityRulesModel.findOneAndUpdate(
                                        {
                                            _id: _id                 //check this related data in collection
                                        },
                                        {
                                            $set: {
                                                tenant_id: tenant_id,
                                                rule_code: rule_code,
                                                weight: weight,
                                                pay_type: 'BOTH',
                                                zone: 'null',
                                                from: from,
                                                to: to,                                // update this are column value                     
                                                list: newlist
                                            }
                                        },
                                        { upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                                        function (err, priorityRules) {
                                            if (err) {
                                                // console.log(err);
                                                res.json({
                                                    status: 400,
                                                    message: 'Some Error Occured During Creation.'
                                                })
                                            } else {
                                                // console.log(priorityRules);
                                                res.json({
                                                    status: 200,
                                                    data: priorityRules._id,
                                                    message: 'save successfully'
                                                });
                                            }
                                        });

                                } catch (error) {
                                    // console.log(`Error: ${error.message}`)
                                    return res.status(500).json({
                                        error: error.message
                                    })
                                }
                            }
                        } else {
                            var newlist = [];
                            req.body.list.forEach(function (data) {
                                if (data.courier_code) {
                                    newlist.push({
                                        courier_code: data.courier_code,
                                    })
                                }
                            })
                            try {
                                var priorityData = new priorityRulesModel({
                                    tenant_id: tenant_id,
                                    rule_code: rule_code,
                                    weight: weight,
                                    pay_type: 'BOTH',
                                    from: from,
                                    to: to,
                                    list: newlist
                                });
                                priorityData.save(function (err, result) {
                                    if (err) {
                                        res.json({
                                            status: 400,
                                            message: 'Some Error Occured During Creation.'
                                        })
                                    } else {
                                        res.json({
                                            status: 200,
                                            data: priorityRules._id,
                                            message: 'save successfully'
                                        });
                                    }
                                });
                            } catch (error) {
                                // console.log(`Error: ${error.message}`)
                                return res.status(500).json({
                                    error: error.message
                                })
                            }
                        }

                    }
                } else if (req.body.list) {
                    var tenant_id = trim(req.body.tenant_id)
                    var newlist = [];
                    req.body.list.forEach(function (data) {
                        if (data.courier_code) {
                            newlist.push({
                                courier_code: data.courier_code,
                            })
                        }
                    })
                    var existsData = []
                    if (req.body._id) {
                        var _id = trim(req.body._id)
                        // existsData = await priorityRulesModel.find({
                        //     _id: _id
                        // })
                        // if (existsData.length != 0) {
                        try {
                            priorityRulesModel.findOneAndUpdate(
                                {
                                    _id: _id
                                },
                                {
                                    $set: {
                                        tenant_id: tenant_id,
                                        rule_code: 'default_rule',
                                        weight: 'null',
                                        pay_type: 'BOTH',
                                        zone: 'null',
                                        from: 'null',
                                        to: 'null',                                // update this are column value                     
                                        list: newlist
                                    }
                                },
                                { upsert: true, new: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                                async function (err, priorityRules) {
                                    if (err) {
                                        // console.log(err);
                                        res.json({
                                            status: 400,
                                            message: 'Some Error Occured During Creation.'
                                        })
                                    } else {
                                        // console.log(priorityRules);
                                        // res.json({
                                        //   status: 200,
                                        //   message: 'save successfully'
                                        // });
                                        if (platform.toUpperCase() == 'V1') {
                                            if (req.body.database && (req.body.database !== undefined || req.body.database !== 'undefined' || req.body.database !== '')) {
                                                var database = trim(req.body.database)
                                                var list = req.body.list
                                                var newlist = [];
                                                req.body.list.forEach(function (data) {
                                                    if (data.courier_code) {
                                                        newlist.push({
                                                            courier_code: data.courier_code,
                                                        })
                                                    }
                                                })
                                                var courier_code = []
                                                var count = 0
                                                newlist.forEach(async function (element) {
                                                    count++
                                                    await courier_code.push({
                                                        courier: element.courier_code,
                                                        priority: count
                                                    })

                                                })
                                                var list = [];
                                                var courier_vendor_list = [];
                                                var courier_master_list = [];
                                                var courier_vendors = await systemDao.reverseCourier()   // check on system db using courier_vendors table

                                                dbController.getMysqlDBClient(database).then(async function (connection2) {
                                                    var sql = "select * from courier_master where is_active=1 AND priority IS NOT NULL order by priority asc";
                                                    connection2.query(sql, (error, results) => {
                                                        if (error) {
                                                            return res.status(400).json({
                                                                message: 'Bad Request',
                                                            })
                                                        } else {
                                                            // console.log("results----",results)
                                                            if (courier_vendors != '') {
                                                                for (let data of courier_vendors) {
                                                                    // console.log("courier_vendors---",data.api_code)
                                                                    courier_vendor_list.push({
                                                                        vendor: data.code
                                                                    })
                                                                }
                                                            } else {
                                                                courier_vendor_list = []
                                                            }

                                                            if (results != '') {
                                                                for (let result of results) {
                                                                    // console.log("results---",result.api_code)
                                                                    courier_master_list.push({
                                                                        master: result.courier_code
                                                                    })
                                                                }
                                                            } else {
                                                                courier_master_list = []
                                                            }

                                                            // console.log("courier_vendor_list--",courier_vendor_list)
                                                            // console.log("courier_master_list--",courier_master_list)
                                                            if (courier_vendor_list.length > 0 && courier_master_list.length > 0) {
                                                                for (var i = 0; i <= courier_vendor_list.length - 1; i++) {
                                                                    console.log("courier_vendor_list@--", courier_vendor_list[i].vendor,)
                                                                    for (var j = 0; j < courier_master_list.length; j++) {
                                                                        if (courier_vendor_list[i] && (courier_vendor_list[i].vendor === courier_master_list[j].master)) {
                                                                            console.log("@@@-----", courier_vendor_list[i].vendor, courier_master_list[j].master)
                                                                            list.push({
                                                                                courier_code: courier_vendor_list[i].vendor
                                                                            })
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                list = []
                                                            }
                                                            if (courier_code.length != 0) {
                                                                courier_code.forEach(async function (element1) {
                                                                    var courier = element1.courier
                                                                    var priority = element1.priority
                                                                    if (courier) {
                                                                        let el = await list.find(itm => itm.courier_code === courier);
                                                                        if (el) {
                                                                            var sql2 = "UPDATE courier_master SET priority = " + priority + ", is_active = " + 1 + " WHERE courier_code = '" + courier + "' AND is_active = 1";
                                                                            connection2.query(sql2, (error, results) => {
                                                                                if (error) {
                                                                                    return res.status(400).json({
                                                                                        message: 'Bad Request',
                                                                                    })
                                                                                } else {
                                                                                    // console.log("results----",results)                              
                                                                                }
                                                                            })
                                                                        }
                                                                    }
                                                                    let el3 = await list.some(item => item.courier_code === courier)
                                                                    // console.log("el3----",el3)
                                                                    if (el3 == false) {
                                                                        // console.log("courier----",courier)
                                                                        var sql3 = "UPDATE courier_master SET priority = " + priority + ", is_active = " + 1 + " WHERE courier_code = '" + courier + "' ";
                                                                        connection2.query(sql3, (error, results) => {
                                                                            if (error) {
                                                                                return res.status(400).json({
                                                                                    message: 'Bad Request',
                                                                                })
                                                                            } else {
                                                                                // console.log("results----",results)                              
                                                                            }
                                                                        })
                                                                    }
                                                                })
                                                            }

                                                            if (list.length != 0) {
                                                                list.forEach(async function (data) {
                                                                    var resultCourier = data.courier_code
                                                                    let el2 = await courier_code.some(item => item.courier === resultCourier)
                                                                    // console.log("el2---",el2)                             
                                                                    if (el2 == false) {
                                                                        var sql2 = "UPDATE courier_master SET is_active = " + 0 + " WHERE courier_code = '" + resultCourier + "' AND is_active = 1";
                                                                        // console.log("sql2---",sql2)
                                                                        connection2.query(sql2, (error, results) => {
                                                                            if (error) {
                                                                                return res.status(400).json({
                                                                                    message: 'Bad Request',
                                                                                })
                                                                            } else {
                                                                                // console.log("results----",results)                              
                                                                            }
                                                                        })
                                                                    } else if (el2 == true) {

                                                                    }
                                                                })
                                                            }



                                                        }
                                                    })
                                                })
                                            } else {
                                                res.json({
                                                    message: 'Database is required....'
                                                })
                                            }
                                        } else if (platform.toUpperCase() == 'V2') {
                                            var newlist = [];
                                            req.body.list.forEach(function (data) {
                                                if (data.courier_code) {
                                                    newlist.push({
                                                        courier_code: data.courier_code,
                                                    })
                                                }
                                            })
                                            let result = newlist.map(a => a.courier_code);
                                            console.log("result---", result)
                                            websiteData = await websites2Model.find({
                                                _id: tenant_id
                                            })
                                            var list = [];
                                            var finalList = [];
                                            if (websiteData != '') {
                                                for (let data of websiteData) {
                                                    console.log("data----", data['reverse_enabled_couriers'])
                                                    for (let element of data['reverse_enabled_couriers']) {
                                                        var isIncluded = result.includes(element);
                                                        if (isIncluded == false) {
                                                            console.log("isIncluded--", isIncluded)
                                                            console.log("element----", element)
                                                            list.push(element)
                                                        } else {
                                                            list = []
                                                        }

                                                    }
                                                    console.log("data----", data['reverse_couriers_history'])
                                                    if (data['reverse_couriers_history']) {
                                                        finalList = data['reverse_couriers_history'].concat(list)
                                                    } else {
                                                        finalList = list
                                                    }
                                                }
                                            } else {
                                                finalList = []
                                            }
                                            console.log("finalList----", finalList)
                                            try {
                                                websites2Model.findOneAndUpdate(
                                                    {
                                                        _id: tenant_id
                                                    },
                                                    {
                                                        $set: {
                                                            reverse_enabled_couriers: result,                          // update this are column value
                                                            reverse_couriers_history: finalList
                                                        }
                                                    },
                                                    { upsert: true, new: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                                                    function (err, priorityRules) {
                                                        if (err) {
                                                            // console.log(err);
                                                            res.json({
                                                                status: 400,
                                                                message: 'Some Error Occured During Creation.'
                                                            })
                                                        } else {
                                                            // console.log(priorityRules);
                                                            // res.json({
                                                            //   status: 200,
                                                            //   message: 'save successfully'
                                                            // });
                                                        }
                                                    });
                                            } catch (error) {
                                                // console.log(`Error: ${error.message}`)
                                                return res.status(500).json({
                                                    error: error.message
                                                })
                                            }
                                        }
                                        res.json({
                                            status: 200,
                                            data: priorityRules._id,
                                            message: 'save successfully'
                                        });
                                    }
                                });

                        } catch (error) {
                            // console.log(`Error: ${error.message}`)
                            return res.status(500).json({
                                error: error.message
                            })
                        }
                        // }
                    } else {
                        var newlist = [];
                        req.body.list.forEach(function (data) {
                            if (data.courier_code) {
                                newlist.push({
                                    courier_code: data.courier_code,
                                })
                            }
                        })
                        try {
                            var priorityData = new priorityRulesModel({
                                tenant_id: tenant_id,
                                pay_type: 'BOTH',
                                rule_code: 'default_rule',
                                list: newlist
                            });
                            priorityData.save(async function (err, priorityRules) {
                                if (err) {
                                    res.json({
                                        status: 400,
                                        message: 'Some Error Occured During Creation.'
                                    })
                                } else {
                                    // res.json({
                                    //   status: 200,
                                    //   message: 'save successfully'
                                    // });
                                    if (platform.toUpperCase() == 'V1') {
                                        if (req.body.database && (req.body.database !== undefined || req.body.database !== 'undefined' || req.body.database !== '')) {
                                            var database = trim(req.body.database)
                                            var list = req.body.list
                                            var newlist = [];
                                            req.body.list.forEach(function (data) {
                                                if (data.courier_code) {
                                                    newlist.push({
                                                        courier_code: data.courier_code,
                                                    })
                                                }
                                            })
                                            var courier_code = []
                                            var count = 0
                                            newlist.forEach(async function (element) {
                                                count++
                                                await courier_code.push({
                                                    courier: element.courier_code,
                                                    priority: count
                                                })

                                            })
                                            var list = [];
                                            var courier_vendor_list = [];
                                            var courier_master_list = [];
                                            var courier_vendors = await systemDao.reverseCourier()   // check on system db using courier_vendors table

                                            dbController.getMysqlDBClient(database).then(async function (connection2) {
                                                var sql = "select * from courier_master where is_active=1 AND priority IS NOT NULL order by priority asc";
                                                connection2.query(sql, (error, results) => {
                                                    if (error) {
                                                        return res.status(400).json({
                                                            message: 'Bad Request',
                                                        })
                                                    } else {
                                                        // console.log("results----",results)
                                                        if (courier_vendors != '') {
                                                            for (let data of courier_vendors) {
                                                                // console.log("courier_vendors---",data.api_code)
                                                                courier_vendor_list.push({
                                                                    vendor: data.code
                                                                })
                                                            }
                                                        } else {
                                                            courier_vendor_list = []
                                                        }

                                                        if (results != '') {
                                                            for (let result of results) {
                                                                // console.log("results---",result.api_code)
                                                                courier_master_list.push({
                                                                    master: result.courier_code
                                                                })
                                                            }
                                                        } else {
                                                            courier_master_list = []
                                                        }

                                                        // console.log("courier_vendor_list--",courier_vendor_list)
                                                        // console.log("courier_master_list--",courier_master_list)
                                                        if (courier_vendor_list.length > 0 && courier_master_list.length > 0) {
                                                            for (var i = 0; i <= courier_vendor_list.length - 1; i++) {
                                                                console.log("courier_vendor_list@--", courier_vendor_list[i].vendor,)
                                                                for (var j = 0; j < courier_master_list.length; j++) {
                                                                    if (courier_vendor_list[i] && (courier_vendor_list[i].vendor === courier_master_list[j].master)) {
                                                                        console.log("@@@-----", courier_vendor_list[i].vendor, courier_master_list[j].master)
                                                                        list.push({
                                                                            courier_code: courier_vendor_list[i].vendor
                                                                        })
                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            list = []
                                                        }
                                                        if (courier_code.length != 0) {
                                                            courier_code.forEach(async function (element1) {
                                                                var courier = element1.courier
                                                                var priority = element1.priority
                                                                if (courier) {
                                                                    let el = await list.find(itm => itm.courier_code === courier);
                                                                    if (el) {
                                                                        var sql2 = "UPDATE courier_master SET priority = " + priority + ", is_active = " + 1 + " WHERE courier_code = '" + courier + "' AND is_active = 1";
                                                                        connection2.query(sql2, (error, results) => {
                                                                            if (error) {
                                                                                return res.status(400).json({
                                                                                    message: 'Bad Request',
                                                                                })
                                                                            } else {
                                                                                // console.log("results----",results)                              
                                                                            }
                                                                        })
                                                                    }
                                                                }
                                                                let el3 = await list.some(item => item.courier_code === courier)
                                                                // console.log("el3----",el3)
                                                                if (el3 == false) {
                                                                    // console.log("courier----",courier)
                                                                    var sql3 = "UPDATE courier_master SET priority = " + priority + ", is_active = " + 1 + " WHERE courier_code = '" + courier + "' ";
                                                                    connection2.query(sql3, (error, results) => {
                                                                        if (error) {
                                                                            return res.status(400).json({
                                                                                message: 'Bad Request',
                                                                            })
                                                                        } else {
                                                                            // console.log("results----",results)                              
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                        if (list.length != 0) {
                                                            list.forEach(async function (data) {
                                                                var resultCourier = data.courier_code
                                                                let el2 = await courier_code.some(item => item.courier === resultCourier)
                                                                // console.log("el2---",el2)                             
                                                                if (el2 == false) {
                                                                    var sql2 = "UPDATE courier_master SET is_active = " + 0 + " WHERE courier_code = '" + resultCourier + "' AND is_active = 1";
                                                                    // console.log("sql2---",sql2)
                                                                    connection2.query(sql2, (error, results) => {
                                                                        if (error) {
                                                                            return res.status(400).json({
                                                                                message: 'Bad Request',
                                                                            })
                                                                        } else {
                                                                            // console.log("results----",results)                              
                                                                        }
                                                                    })
                                                                } else if (el2 == true) {

                                                                }
                                                            })
                                                        }



                                                    }
                                                })
                                            })
                                        } else {
                                            res.json({
                                                message: 'Database is required....'
                                            })
                                        }
                                    } else if (platform.toUpperCase() == 'V2') {
                                        var newlist = [];
                                        req.body.list.forEach(function (data) {
                                            if (data.courier_code) {
                                                newlist.push({
                                                    courier_code: data.courier_code,
                                                })
                                            }
                                        })
                                        let result = newlist.map(a => a.courier_code);
                                        console.log("result---", result)
                                        websiteData = await websites2Model.find({
                                            _id: tenant_id
                                        })
                                        var list = [];
                                        var finalList = [];
                                        if (websiteData != '') {
                                            for (let data of websiteData) {
                                                console.log("data----", data['reverse_enabled_couriers'])
                                                for (let element of data['reverse_enabled_couriers']) {
                                                    var isIncluded = result.includes(element);
                                                    if (isIncluded == false) {
                                                        console.log("isIncluded--", isIncluded)
                                                        console.log("element----", element)
                                                        list.push(element)
                                                    } else {
                                                        list = []
                                                    }

                                                }
                                                console.log("data----", data['reverse_couriers_history'])
                                                if (data['reverse_couriers_history']) {
                                                    finalList = data['reverse_couriers_history'].concat(list)
                                                } else {
                                                    finalList = list
                                                }
                                            }
                                        } else {
                                            finalList = []
                                        }
                                        console.log("finalList----", finalList)
                                        try {
                                            websites2Model.findOneAndUpdate(
                                                {
                                                    _id: tenant_id
                                                },
                                                {
                                                    $set: {
                                                        reverse_enabled_couriers: result,                          // update this are column value
                                                        reverse_couriers_history: finalList
                                                    }
                                                },
                                                { upsert: true, new: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                                                function (err, priorityRules) {
                                                    if (err) {
                                                        // console.log(err);
                                                        res.json({
                                                            status: 400,
                                                            message: 'Some Error Occured During Creation.'
                                                        })
                                                    } else {
                                                        // console.log(priorityRules);
                                                        // res.json({
                                                        //   status: 200,
                                                        //   message: 'save successfully'
                                                        // });
                                                    }
                                                });
                                        } catch (error) {
                                            // console.log(`Error: ${error.message}`)
                                            return res.status(500).json({
                                                error: error.message
                                            })
                                        }
                                    }
                                    res.json({
                                        status: 200,
                                        data: priorityRules._id,
                                        message: 'save successfully'
                                    });
                                }

                            });
                        } catch (error) {
                            // console.log(`Error: ${error.message}`)
                            return res.status(500).json({
                                error: error.message
                            })
                        }
                    }
                    // res.json({
                    //   status: 200,
                    //   message: 'save successfully'
                    // });
                } else {
                    res.json({
                        message: 'Required all fields....'
                    })
                }
            }
        } else {
            res.json({
                message: 'Platfrom is Required....'
            })
        }
    }
}

function sleep(ms) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  }

exports.getPriorityRules = async (req, res, next) => {
    await sleep(2000)
    try {
        if (req.body.tenant_id && (req.body.tenant_id !== undefined || req.body.tenant_id !== 'undefined' || req.body.tenant_id !== '')) {

            var tenant_id = trim(req.body.tenant_id)
            // console.log("tenant_id--",tenant_id)
            const priorityRules = await priorityRulesModel.find({
                tenant_id: tenant_id,                      //check tenant_id related data in collection
            });
            // console.log("priorityRules---", priorityRules)
            var activeData = []
            if (priorityRules.length == 1) {
                console.log("inactive")
                activeData.push({
                    status: 'Inactive'
                })
            } else if (priorityRules.length > 1) {
                console.log("active")
                activeData.push({
                    status: 'Active'
                })
            }
            if (priorityRules != '') {
                res.json({
                    status: 200,
                    message: 'success',
                    data: priorityRules,
                    data1: activeData
                })
            } else {
                res.json({
                    status: 400,
                    message: 'Not Data Found'
                })
            }
        } else {
            res.json({
                status: 400,
                message: 'tenant_id are required....'
            })
        }

    } catch (error) {
        // console.log(`Error: ${error.message}`)
        return res.status(500).json({
            error: error.message
        })
    }
}

  /*
    Search client name of v1 and v2 platform api [v1 use system master (mysql)database and hostnames collection fqdn column]
    [v2 shiplink_staging (mongodb)database websites table]
*/
exports.searchClientName = async (req, res, next) => {
    if (req.body.client_name && (req.body.client_name !== undefined || req.body.client_name !== 'undefined' || req.body.client_name !== '')) {
        var client_name = trim(req.body.client_name)
        var allClient = [];
        try {
            const searchData = await websites2Model.find({ 'mainvendor': { '$regex': client_name } })  // V2 client_name check
            // console.log("searchData---",searchData)
            if (searchData != '') {
                for (let data of searchData) {
                    allClient.push({                               // V2 clients data push on array
                        platform: 'V2',
                        client_name: data.mainvendor,
                        tenant_id: data._id,
                        database_name: 'instashipin'
                    })
                }
            } else {
                allClient == allClient
            }
            var searchDataV1 = await systemDao.searchData(client_name)
            if (searchDataV1 != '') {
                for (let data of searchDataV1) {
                    allClient.push({                            // V1 clients data push on array
                        platform: data.platform,
                        client_name: data.client_name,
                        tenant_id: data.tenant_id.toString(),
                        database_name: data.database_name
                    })
                }
            } else {
                allClient == allClient
            }
            if (allClient != '') {
                res.json({
                  status: 200,
                  message: 'Success',
                  data: allClient
                })
              } else {
                res.json({
                  status: 400,
                  message: 'Not matching data',
                  data: []
                })
              }
        } catch (error) {
            return res.status(500).json({
                status: false,
                error: error.message
            })
        }
    } else {
        res.json({
            status: 400,
            message: 'Please enter client name....'
        })
    }
}

/*
    Validation api for checking the added data already exists or not.
*/

exports.validationPriorityRules = async (req, res, next) => {
    if (req.body.tenant_id == undefined) {
        res.json({
            message: 'Required all fields....'
        })
    } else {
        if (req.body.pay_type != undefined) {
            if (req.body.rule_code == 'WZ') {
                if (req.body.weight == undefined || req.body.zone == undefined || req.body.list == undefined) {
                    res.json({
                        message: 'Validation: Required zone, weight and priority...'
                    })
                } else {
                    var tenant_id = trim(req.body.tenant_id)
                    var pay_type = trim(req.body.pay_type)
                    var rule_code = trim(req.body.rule_code)
                    var zone = trim(req.body.zone)
                    var weight = trim(req.body.weight)
                    var arr = weight.split("-");
                    var from = trim(arr[0])
                    var to = trim(arr[1])
                    try {
                        const results = await priorityRulesModel.find({
                            tenant_id: tenant_id,            //check this related data in collection
                            rule_code: rule_code,
                            weight: weight,
                            pay_type: pay_type,
                            zone: zone,
                            from: from,
                            to: to
                        })
                        if (results != '') {
                            res.json({
                                error: true,
                                message: 'Data Already Exist'
                            });
                        } else {
                            res.json({
                                error: false,
                            });
                        }
                    } catch (error) {
                        // console.log(`Error: ${error.message}`)
                        return res.status(500).json({
                            error: error.message
                        })
                    }
                }
            } else if (req.body.rule_code == 'ZM') {
                if (req.body.zone == undefined || req.body.list == undefined) {
                    res.json({
                        message: 'Validation: Required zone and priority...'
                    })
                } else {
                    var tenant_id = trim(req.body.tenant_id)
                    var pay_type = trim(req.body.pay_type)
                    var rule_code = trim(req.body.rule_code)
                    var zone = trim(req.body.zone)
                    try {
                        const results = await priorityRulesModel.find({
                            tenant_id: tenant_id,            //check this related data in collection
                            rule_code: rule_code,
                            pay_type: pay_type,
                            zone: zone,
                        })
                        if (results != '') {
                            res.json({
                                error: true,
                                message: 'Data Already Exist'
                            });
                        } else {
                            res.json({
                                error: false,
                            });
                        }
                    } catch (error) {
                        // console.log(`Error: ${error.message}`)
                        return res.status(500).json({
                            error: error.message
                        })
                    }
                }
            } else if (req.body.rule_code == 'WM') {
                if (req.body.weight == undefined || req.body.list == undefined) {
                    res.json({
                        message: 'Validation: Required weight and priority...'
                    })
                } else {
                    var tenant_id = trim(req.body.tenant_id)
                    var pay_type = trim(req.body.pay_type)
                    var rule_code = trim(req.body.rule_code)
                    var weight = trim(req.body.weight)
                    var arr = weight.split("-");
                    var from = trim(arr[0])
                    var to = trim(arr[1])
                    try {
                        const results = await priorityRulesModel.find({
                            tenant_id: tenant_id,            //check this related data in collection
                            rule_code: rule_code,
                            weight: weight,
                            pay_type: pay_type,
                            from: from,
                            to: to
                        })
                        if (results != '') {
                            res.json({
                                error: true,
                                message: 'Data Already Exist'
                            });
                        } else {
                            res.json({
                                error: false,
                            });
                        }
                    } catch (error) {
                        // console.log(`Error: ${error.message}`)
                        return res.status(500).json({
                            error: error.message
                        })
                    }
                }
            } else if (req.body.list) {
                var tenant_id = trim(req.body.tenant_id)
                var pay_type = trim(req.body.pay_type)
                try {
                    const results = await priorityRulesModel.find({
                        tenant_id: tenant_id,
                        pay_type: pay_type,
                        rule_code: 'default_rule'
                    })
                    if (results != '') {
                        res.json({
                            error: true,
                            message: 'Data Already Exist'
                        });
                    } else {
                        res.json({
                            error: false,
                        });
                    }
                } catch (error) {
                    // console.log(`Error: ${error.message}`)
                    return res.status(500).json({
                        error: error.message
                    })
                }
            } else {
                res.json({
                    message: 'Required all fields....'
                })
            }
        } else if (req.body.pay_type == undefined) {
            if (req.body.rule_code == 'WZ') {
                if (req.body.weight == undefined || req.body.zone == undefined || req.body.list == undefined) {
                    res.json({
                        message: 'Validation: Required zone, weight and priority...'
                    })
                } else {
                    var tenant_id = trim(req.body.tenant_id)
                    var rule_code = trim(req.body.rule_code)
                    var zone = trim(req.body.zone)
                    var weight = trim(req.body.weight)
                    var arr = weight.split("-");
                    var from = trim(arr[0])
                    var to = trim(arr[1])
                    try {
                        const results = await priorityRulesModel.find({
                            tenant_id: tenant_id,            //check this related data in collection
                            rule_code: rule_code,
                            weight: weight,
                            pay_type: 'BOTH',
                            zone: zone,
                            from: from,
                            to: to
                        })
                        if (results != '') {
                            res.json({
                                error: true,
                                message: 'Data Already Exist'
                            });
                        } else {
                            res.json({
                                error: false
                            });
                        }
                    } catch (error) {
                        // console.log(`Error: ${error.message}`)
                        return res.status(500).json({
                            error: error.message
                        })
                    }
                }
            } else if (req.body.rule_code == 'ZM') {
                if (req.body.zone == undefined || req.body.list == undefined) {
                    res.json({
                        message: 'Validation: Required zone and priority...'
                    })
                } else {
                    var tenant_id = trim(req.body.tenant_id)
                    var rule_code = trim(req.body.rule_code)
                    var zone = trim(req.body.zone)
                    try {
                        const results = await priorityRulesModel.find({
                            tenant_id: tenant_id,            //check this related data in collection
                            rule_code: rule_code,
                            pay_type: 'BOTH',
                            zone: zone,
                        })
                        if (results != '') {
                            res.json({
                                error: true,
                                message: 'Data Already Exist'
                            });
                        } else {
                            res.json({
                                error: false,
                            });
                        }
                    } catch (error) {
                        // console.log(`Error: ${error.message}`)
                        return res.status(500).json({
                            error: error.message
                        })
                    }
                }
            } else if (req.body.rule_code == 'WM') {
                if (req.body.weight == undefined || req.body.list == undefined) {
                    res.json({
                        message: 'Validation: Required weight and priority...'
                    })
                } else {
                    var tenant_id = trim(req.body.tenant_id)
                    var rule_code = trim(req.body.rule_code)
                    var weight = trim(req.body.weight)
                    var arr = weight.split("-");
                    var from = trim(arr[0])
                    var to = trim(arr[1])
                    try {
                        const results = await priorityRulesModel.find({
                            tenant_id: tenant_id,            //check this related data in collection
                            rule_code: rule_code,
                            weight: weight,
                            pay_type: 'BOTH',
                            from: from,
                            to: to
                        })
                        if (results != '') {
                            res.json({
                                error: true,
                                message: 'Data Already Exist'
                            });
                        } else {
                            res.json({
                                error: false,
                            });
                        }
                    } catch (error) {
                        // console.log(`Error: ${error.message}`)
                        return res.status(500).json({
                            error: error.message
                        })
                    }
                }
            } else if (req.body.list) {
                var tenant_id = trim(req.body.tenant_id)
                try {
                    const results = await priorityRulesModel.find({
                        tenant_id: tenant_id,
                        pay_type: 'BOTH',
                        rule_code: 'default_rule'
                    })
                    if (results != '') {
                        res.json({
                            error: true,
                            message: 'Data Already Exist'
                        });
                    } else {
                        res.json({
                            error: false,
                        });
                    }
                } catch (error) {
                    // console.log(`Error: ${error.message}`)
                    return res.status(500).json({
                        error: error.message
                    })
                }
            } else {
                res.json({
                    message: 'Required all fields....'
                })
            }
        }
    }
}

/*
    This api used for deleted priority rules by using its tenant_id and _id.
*/

exports.removePriorityRules = async (req, res) => {
    try {
        if (req.body.tenant_id && (req.body.tenant_id !== undefined || req.body.tenant_id !== 'undefined' || req.body.tenant_id !== '') &&
            req.body._id && (req.body._id !== undefined || req.body._id !== 'undefined' || req.body._id !== '')) {
            var _id = trim(req.body._id)
            var tenant_id = trim(req.body.tenant_id)
            var removeData = []
            var query = {
                _id: _id,
                tenant_id: tenant_id
            }
            removeData = await priorityRulesModel.find({
                _id: _id,
                tenant_id: tenant_id
            })
            if (removeData.length != 0) {
                priorityRulesModel.findOneAndRemove(query).exec(function (err, priorityData) {
                    if (err) {
                        return res.status(500).json({
                            message: 'Internal server error!!!....'
                        });
                    } else {
                        res.json({
                            status: 200,
                            data: priorityData,
                            message: 'Deleted Successfully!!!....'
                        });
                    }

                });
            } else if (removeData.length == 0) {
                res.json({
                    status: 400,
                    message: 'Bad Request!!!....'
                });
            }
        } else {
            res.json({
                message: 'Select the rule you want to deleted!!!....'
            });
        }

    } catch (error) {
        // console.log(`Error: ${error.message}`)
        return res.status(500).json({
            error: error.message
        })
    }


}

/*
    This route used for get v1(using client db in mysql) and v2(defaultly connected mongodb) platforms couriers data ....(default priority rule).
    If changes in defaults priority rule (if, is_active = 0 and rp = 1 or deleted this couriers) then this changes are updated on related couriers priority rules.
*/
exports.getCourierData = async (req, res, next) => {
  // console.log("req.body---",req.body)
  if (req.body.tenant_id && (req.body.tenant_id !== undefined || req.body.tenant_id !== 'undefined' || req.body.tenant_id !== '')) {
    // if (req.body.database && (req.body.database !== undefined || req.body.database !== 'undefined' || req.body.database !== '')) {
    if (req.body.platform && (req.body.platform !== undefined || req.body.platform !== 'undefined' || req.body.platform !== '')) {
      var tenant_id = trim(req.body.tenant_id)
      var platform = trim(req.body.platform)
      if (platform.toUpperCase() == 'V1') {
        if (req.body.database && (req.body.database !== undefined || req.body.database !== 'undefined' || req.body.database !== '')) {
          var database = trim(req.body.database)
          var list = [];
          var courier_vendor_list = [];
          var courier_master_list = [];
          var courier_vendors = await systemDao.reverseCourier()   // check on system db using courier_vendors table

          var sql = 'select * from courier_master where is_active=1 AND priority IS NOT NULL order by priority asc';
          var connection2 = await dbClient.getMysqlDBClient(database.toString())    //check on perticular client db wise us courier_master table
          connection2.query(sql, (error, results) => {
            if (error) {
              return res.status(400).json({
                message: 'Bad Request',
              })
            } else {
                // console.log("courier_vendors--",courier_vendors.length)
                // console.log("results---",results.length)
                if(courier_vendors != ''){
                    for (let data of courier_vendors) {
                        // console.log("courier_vendors---",data.api_code)
                        courier_vendor_list.push({
                            vendor: data.code
                        })
                    }
                }else{
                    courier_vendor_list = []
                }
               
                if(results != ''){
                    for (let result of results) {
                        // console.log("results---",result.api_code)
                        courier_master_list.push({
                            master: result.courier_code
                        })
                    }
                }else{
                    courier_master_list = []
                }
               
                // console.log("courier_vendor_list--",courier_vendor_list)
                // console.log("courier_master_list--",courier_master_list)
                if(courier_vendor_list.length > 0 &&  courier_master_list.length > 0){
                    for (var i = 0; i <= courier_vendor_list.length - 1; i++) {
                        console.log("courier_vendor_list@--",courier_vendor_list[i].vendor, )
                        for (var j = 0; j < courier_master_list.length; j++) {
                          if (courier_vendor_list[i] && (courier_vendor_list[i].vendor === courier_master_list[j].master)) {
                            console.log("@@@-----",courier_vendor_list[i].vendor,courier_master_list[j].master)
                            list.push({
                              courier_code: courier_vendor_list[i].vendor
                            })
                          }
                        }
                      }  
                }else{
                    list = []
                }
                 console.log("list---",list.length)  
              if (list.length != 0) {
                try {
                 
                  priorityRulesModel.findOneAndUpdate(
                    {
                      tenant_id: tenant_id,
                      rule_code: 'default_rule'
                    },
                    {
                      $set: {
                        list: list                           // update this are column value
                      }
                    },
                    { new: true, upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                    async function (err, priorityRules) {
                      // console.log("priorityRules---", priorityRules)
                      if (err) {
                        // console.log(err);
                        res.json({
                          status: 400,
                          message: 'Some Error Occured During Creation.'
                        })
                      } else {
                        var tenant_id = trim(req.body.tenant_id)
                        const priorityData = await priorityRulesModel.find({
                          tenant_id: tenant_id,
                          rule_code: { "$ne": 'default_rule' }
                        })
                        for await (let element of priorityData) {
                          var newdata = []
                          for (var i = 0; i <= element.list.length - 1; i++) {
                            for (var j = 0; j < list.length; j++) {
                              if (element.list[i] && (element.list[i].courier_code === list[j].courier_code)) {
                                // element.list.splice(i, 1);
                                console.log("element.list[i].courier_code---", element.list[i].courier_code)
                                newdata.push({
                                  courier_code: element.list[i].courier_code
                                })
                                // element.list.splice(element.list.indexOf(i), 1);
                                console.log("element.list---", element.list)
                              }
                            }
                          }
                          priorityRulesModel.findOneAndUpdate(
                            {
                                tenant_id: element.tenant_id,
                                rule_code: element.rule_code,
                                pay_type: element.pay_type,
                                zone: element.zone,
                                weight: element.weight,
                                from: element.from, 
                                to: element.to
                            },
                            {
                                $set: {
                                    list: newdata                           // update this are column value
                                }
                            },
                            { new: true, upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                            async function (err, priorityRules) {
                              if (err) {
                                // console.log(err);
                                res.json({
                                  status: 400,
                                  message: 'Some Error Occured During Updation.'
                                })
                              } else {
                                // res.json({
                                //   status: 200,
                                //   message: 'success',
                                // })
                              }
                            })

                        }
                        res.json({
                          status: 200,
                          message: 'success',
                          data: priorityRules
                        })
                      }
                    });
                } catch (error) {
                  return res.status(500).json({
                    status: false,
                    message: 'Server Error'
                  })
                }
              } else {
                  console.log("reacher hear")
                priorityRulesModel.findOneAndUpdate(
                  {
                    tenant_id: tenant_id,
                    rule_code: 'default_rule'
                  },
                  {
                    $set: {
                      list: list                           // update this are column value
                    }
                  },
                  { new: true, upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                  async function (err, priorityRules) {
                    if (err) {
                      // console.log(err);
                      res.json({
                        status: 400,
                        message: 'Some Error Occured During Creation.'
                      })
                    } else {
                      var tenant_id = trim(req.body.tenant_id)
                      const priorityData = await priorityRulesModel.find({
                        tenant_id: tenant_id
                      })
                    //   console.log("priorityData---",priorityData)
                      var newdata = [];
                      var query = {
                        tenant_id: tenant_id,
                      },
                      update = {
                        $set: {
                          list: newdata,
                        },
                      };
                      priorityRulesModel.updateMany(query, update, function (err, priority) {
                      if (err) {
                          res.json({
                              status: 400,
                              message: 'Some Error Occured During Updation.'
                          })
                      }
                    });
                    //   for await (let element of priorityData) {
                    //       console.log("pay_type----",element.pay_type)
                    //      await priorityRulesModel.findOneAndUpdate(
                    //           {
                    //             tenant_id: element.tenant_id,
                    //             rule_code: element.rule_code,
                    //             pay_type: element.pay_type,
                    //             zone: element.zone,
                    //             weight: element.weight,
                    //             from: element.from,
                    //             to: element.to
                    //           },
                    //           {
                    //               $set: {
                    //                   list: newdata                           // update this are column value
                    //               }
                    //           },
                    //           { new: true, upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                    //           async function (err, priorityRules) {
                    //               if (err) {
                    //                   // console.log(err);
                    //                   res.json({
                    //                       status: 400,
                    //                       message: 'Some Error Occured During Updation.'
                    //                   })
                    //               } else {
                    //                   // res.json({
                    //                   //   status: 200,
                    //                   //   message: 'success',
                    //                   // })
                    //               }
                    //           })
                    //   }
                      res.json({
                        status: 200,
                        message: 'success',
                        data: priorityRules
                      })
                    }
                  });
              }
            }
          })

        } else {
          res.json({
            status: 400,
            message: 'Validation: database is required...'
          })
        }
      } else if (platform.toUpperCase() == 'V2') {
        var v2list = [];
        var tenant_id = trim(req.body.tenant_id)
        try {
          const websites2Data = await websites2Model.find({
            _id: tenant_id
          })
          if (websites2Data != '') {
            for (let data of websites2Data) {
              var count = 1
              for (let element of data.reverse_enabled_couriers) {
                v2list.push({
                  // priority: count,
                  courier_code: element
                })
                count++
              }
              // console.log("v2list----",v2list) 
            }
            var tenant_id = trim(req.body.tenant_id)
            priorityRulesModel.findOneAndUpdate(
              {
                tenant_id: tenant_id,
                rule_code: 'default_rule'
              },
              {
                $set: {
                  list: v2list                           // update this are column value
                }
              },
              { new: true, upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
              async function (err, priorityRules) {
                if (err) {
                  // console.log(err);
                  res.json({
                    status: 400,
                    message: 'Some Error Occured During Creation.'
                  })
                } else {
                  var tenant_id = trim(req.body.tenant_id)
                  // console.log("tenant_id$$---",tenant_id)
                  const priorityData = await priorityRulesModel.find({
                    tenant_id: tenant_id,
                    rule_code: { "$ne": 'default_rule' }
                  })
                  for await (let element of priorityData) {
                    var newdata = []
                    for (var i = 0; i <= element.list.length - 1; i++) {
                      for (var j = 0; j < v2list.length; j++) {
                        if (element.list[i] && (element.list[i].courier_code === v2list[j].courier_code)) {
                          // element.list.splice(i, 1);
                          console.log("element.list[i].courier_code---", element.list[i].courier_code)
                          newdata.push({
                            courier_code: element.list[i].courier_code
                          })
                          // element.list.splice(element.list.indexOf(i), 1);
                          console.log("element.list---", element.list)
                        }
                      }
                    }
                    console.log("newdata--", newdata)
                    priorityRulesModel.findOneAndUpdate(
                      {
                        tenant_id: element.tenant_id,
                        rule_code: element.rule_code,
                        pay_type: element.pay_type,
                        zone: element.zone,
                        weight: element.weight,
                        from: element.from,
                        to: element.to
                      },
                      {
                        $set: {
                          list: newdata                           // update this are column value
                        }
                      },
                      { new: true, upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                      async function (err, priorityRules) {
                        if (err) {
                          // console.log(err);
                          res.json({
                            status: 400,
                            message: 'Some Error Occured During Updation.'
                          })
                        } else {
                          // res.json({
                          //   status: 200,
                          //   message: 'success',
                          // })
                        }
                      })

                  }
                  res.json({
                    status: 200,
                    message: 'success',
                    data: priorityRules
                  })
                }
              });
          } else {
            var tenant_id = trim(req.body.tenant_id)
            priorityRulesModel.findOneAndUpdate(
              {
                tenant_id: tenant_id,
                rule_code: 'default_rule'
              },
              {
                $set: {
                  list: v2list                           // update this are column value
                }
              },
              { new: true, upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
              async function (err, priorityRules) {
                if (err) {
                  // console.log(err);
                  res.json({
                    status: 400,
                    message: 'Some Error Occured During Creation.'
                  })
                } else {
                  var tenant_id = trim(req.body.tenant_id)
                  // console.log("tenant_id$$---",tenant_id)
                  const priorityData = await priorityRulesModel.find({
                    tenant_id: tenant_id
                  })
                  var newdata = [];
                  var query = {
                    tenant_id: tenant_id,
                  },
                  update = {
                    $set: {
                      list: newdata,
                    },
                  };
                  priorityRulesModel.updateMany(query, update, function (err, priority) {
                  if (err) {
                      res.json({
                          status: 400,
                          message: 'Some Error Occured During Updation.'
                      })
                  }
                });
                //   for await (let element of priorityData) {
                //     priorityRulesModel.findOneAndUpdate(
                //       {
                //         tenant_id: element.tenant_id,
                //         rule_code: element.rule_code,
                //         pay_type: element.pay_type,
                //         zone: element.zone,
                //         weight: element.weight,
                //         from: element.from,
                //         to: element.to
                //       },
                //       {
                //         $set: {
                //           list: newdata                           // update this are column value
                //         }
                //       },
                //       { new: true, upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                //       async function (err, priorityRules) {
                //         if (err) {
                //           // console.log(err);
                //           res.json({
                //             status: 400,
                //             message: 'Some Error Occured During Updation.'
                //           })
                //         } else {
                //           // res.json({
                //           //   status: 200,
                //           //   message: 'success',
                //           // })
                //         }
                //       })
                //   }
                  res.json({
                    status: 200,
                    message: 'success',
                    data: priorityRules
                  })
                }
              });
          }

        } catch (error) {
          return res.status(500).json({
            status: false,
            error: error.message
          })
        }

      }
    } else {
      res.json({
        status: 400,
        message: 'Validation: Platform is required...'
      })
    }
  } else {
    res.json({
      status: 400,
      message: 'Validation: All field are required...'
    })
  }



}

/*
    This route get pincode wise checked all zones in list and its related zone wise priority rule.
 */
exports.getZones = async (req, res, next) => {
    if (req.body.pickupPincode && req.body.deliveryPincode && req.body.platform && (req.body.pickupPincode !== undefined || req.body.pickupPincode !== 'undefined' || req.body.pickupPincode !== '') && (req.body.deliveryPincode !== undefined || req.body.deliveryPincode !== 'undefined' || req.body.deliveryPincode !== '') && (req.body.platform !== undefined || req.body.platform !== 'undefined' || req.body.platform !== '')) {
        var zone_list = [];
        var pickupPincode = trim(req.body.pickupPincode)
        var deliveryPincode = trim(req.body.deliveryPincode)
        let zone = '';
        var pincode_master = await ilogix_standard_pincodes_master.getIlogixStandardPincodes(pickupPincode, deliveryPincode)
        if (pincode_master && (pincode_master != '' && pincode_master.length >= 2)) {
            // console.log("pincode_master--",pincode_master)
            var pickupCity = pincode_master.find(x => x.pincode === pickupPincode).city_id;
            //  console.log("pickupCity----",pickupCity)
            var deliveryCity = pincode_master.find(x => x.pincode === deliveryPincode).city_id;
            // console.log("deliveryCity---",deliveryCity)
            var pickupState = pincode_master.find(x => x.pincode === pickupPincode).state_id;
            //  console.log("pickupState----",pickupState)
            var deliveryState = pincode_master.find(x => x.pincode === deliveryPincode).state_id;
            if (pickupCity && deliveryCity && pickupCity.toLowerCase() === deliveryCity.toLowerCase() && pickupState.toLowerCase() === deliveryState.toLowerCase()) {
                zone_code = 'IC';
                var consignor_zone_data = await consignor_standard_billing_zones.getZone(zone_code)
                if (consignor_zone_data != '') {
                    console.log("Intra City @@@@", consignor_zone_data)
                    zone = consignor_zone_data[0].zone;
                    await zone_list.push({
                        zone: zone,
                    })
                } else {
                    zone_list = zone_list
                }
            } else if (pickupCity && deliveryCity && pickupCity.toLowerCase() != deliveryCity.toLowerCase() && pickupState.toLowerCase() === deliveryState.toLowerCase()) {
                zone_code = 'IS';
                var consignor_zone_data = await consignor_standard_billing_zones.getZone(zone_code)
                console.log("Intra state @@@@", consignor_zone_data)
                if (consignor_zone_data != '') {
                    zone = consignor_zone_data[0].zone;
                    await zone_list.push({
                        zone: zone,
                    })
                } else {
                    zone_list = zone_list
                }
            } else {
                zone_list = zone_list
            }

        } else {
            zone_list = zone_list
        }

        // console.log("zonelist1---", zone_list)
        // NJ Special Zone check
        console.log("nj zone checked.....")
        var zone_main = ['IC', 'IS'];
        var zone_code1 = '';
        var customList = await consignor_standard_billing_zones.customDeliveryList()
        // console.log("customList---",customList)
        for (let czone of customList) {
            zone_code1 = czone.zone_code;
        }
        var otherPincodeZone = await ilogix_standard_other_pincodes.getOtherPincodeZone(zone_code1, deliveryPincode)
        // console.log("otherPincodeZone---",otherPincodeZone)
        if (otherPincodeZone != '') {
            var custom_pickup_pincodes = [];
            for (let cpincode of otherPincodeZone) {
                custom_pickup_pincodes.push(cpincode.pincode)
            }
            const found1 = custom_pickup_pincodes.some(r => deliveryPincode.includes(r))
            if (found1 !== false) {

                zone_code = zone_code1;
                var consignor_zone_data = await consignor_standard_billing_zones.getZone(zone_code)
                console.log("otherPincodeZone---@@@@@@@")
                if (consignor_zone_data != '') {
                    zone = consignor_zone_data[0].zone;
                    await zone_list.push({
                        zone: zone,
                    })
                }
            } else {
                zone_list = zone_list
            }
        } else {
            zone_list = zone_list
        }

        // console.log("zonelist2---", zone_list)
        var westZone = [{ zone_code: 'WZ' }];
        var zone_code2 = '';
        for (let czone of westZone) {
            zone_code2 = czone.zone_code
        }
        var westZoneData = await ilogix_standard_other_pincodes.getOtherPincodeZone2(zone_code2, pickupPincode, deliveryPincode)
        if (westZoneData != '' && westZoneData.length > 2) {
            // console.log("reached on west zone @@@@@@--",westZoneData)
            var westPincode = [];
            for (let cpincode of westZoneData) {
                westPincode.push(cpincode.pincode)
            }
            const found2 = westPincode.some(r => pickupPincode.includes(r))
            const found3 = westPincode.some(r => deliveryPincode.includes(r))
            if (found2 !== false && found3 !== false) {
                zone_code = zone_code2

                var consignor_zone_data = await consignor_standard_billing_zones.getZone(zone_code)
                console.log("westZone---@@@@@@@")
                if (consignor_zone_data != '') {
                    zone = consignor_zone_data[0].zone;
                    await zone_list.push({
                        zone: zone,
                    })
                } else {
                    zone_list = zone_list
                }
            }

        } else if (westZoneData.length < 2 || westZoneData == '') {
            zone_list = zone_list
        }

        // console.log("zonelist3---", zone_list)
        // East zone check
        var eastZone = [{ zone_code: 'EZ' }]
        var zone_code3 = '';
        for (let czone of eastZone) {
            zone_code3 = czone.zone_code
        }
        var eastZoneData = await ilogix_standard_other_pincodes.getOtherPincodeZone2(zone_code3, pickupPincode, deliveryPincode)
        if (eastZoneData != '' && eastZoneData.length > 2) {
            var eastPincode = [];
            for (let cpincode of eastZoneData) {
                eastPincode.push(cpincode.pincode)
            }
            const found4 = eastPincode.some(r => pickupPincode.includes(r))
            const found5 = eastPincode.some(r => deliveryPincode.includes(r))
            if (found4 !== false && found5 !== false) {
                zone_code = zone_code3;
                var consignor_zone_data = await consignor_standard_billing_zones.getZone(zone_code)
                console.log("eastzone---@@@@@@@")
                if (consignor_zone_data != '') {
                    zone = consignor_zone_data[0].zone;
                    await zone_list.push({
                        zone: zone,
                    })
                } else {
                    zone_list = zone_list
                }
            }
        } else if (eastZoneData.length < 2 || eastZoneData == '') {
            zone_list = zone_list
        }

        // console.log("zonelist4---", zone_list)
        // North zone check
        var northZone = [{ zone_code: 'NZ' }];
        var zone_code4 = ''
        for (let czone of northZone) {
            zone_code4 = czone.zone_code
        }
        var northZoneData = await ilogix_standard_other_pincodes.getOtherPincodeZone2(zone_code4, pickupPincode, deliveryPincode)
        if (northZoneData != '' && northZoneData.length > 2) {
            var northPincode = [];
            for (let cpincode of northZoneData) {
                northPincode.push(cpincode.pincode)
            }
            const found6 = northPincode.some(r => deliveryPincode.includes(r))
            const found7 = northPincode.some(r => deliveryPincode.includes(r))
            if (found6 !== false && found7 !== false) {
                zone_code = zone_code4
                var consignor_zone_data = await consignor_standard_billing_zones.getZone(zone_code)
                console.log("northZone---@@@@@@@")
                if (consignor_zone_data != '') {
                    zone = consignor_zone_data[0].zone;
                    await zone_list.push({
                        zone: zone,
                    })
                } else {
                    zone_list = zone_list
                }
            }
        } else if (northZoneData.length < 2 || northZoneData == '') {
            zone_list = zone_list
        }

        // console.log("zonelist5---", zone_list)
        //South zone check
        var southZone = [{ zone_code: 'SZ' }];
        var zone_code5 = '';
        for (let czone of southZone) {
            zone_code5 = czone.zone_code
        }
        var southZoneData = await ilogix_standard_other_pincodes.getOtherPincodeZone2(zone_code5, pickupPincode, deliveryPincode)
        if (southZoneData != '' && southZoneData.length > 2) {
            var southPincode = [];
            for (let cpincode of southZoneData) {
                southPincode.push(cpincode.pincode)
            }
            const found8 = southPincode.some(r => pickupPincode.includes(r))
            const found9 = southPincode.some(r => deliveryPincode.includes(r))
            if (found8 !== false && found9 !== false) {
                zone_code = zone_code5
                var consignor_zone_data = await consignor_standard_billing_zones.getZone(zone_code)
                console.log("southZone---@@@@@@@")
                if (consignor_zone_data != '') {
                    zone = consignor_zone_data[0].zone;
                    await zone_list.push({
                        zone: zone,
                    })
                } else {
                    zone_list = zone_list
                }
            }

        } else if (southZoneData.length < 2 || southZoneData == '') {
            zone_list = zone_list
        }

        // console.log("zonelist6---", zone_list)
        // Metro to Metro zone check
        var metroZone = [{ zone_code: 'MM' }];
        var zone_code6 = '';
        for (let czone of metroZone) {
            zone_code6 = czone.zone_code
        }
        var metroZoneData = await ilogix_standard_other_pincodes.getMetroZone(zone_code6, pickupPincode, deliveryPincode)
        if (metroZoneData != '' && metroZoneData.length > 2) {
            var metroPincode = [];
            for (let cpincode of metroZoneData) {
                metroPincode.push(cpincode.pincode)
            }
            const found10 = metroPincode.some(r => pickupPincode.includes(r))
            const found11 = metroPincode.some(r => deliveryPincode.includes(r))
            if (found10 !== false && found11 !== false) {
                zone_code = zone_code6
                var consignor_zone_data = await consignor_standard_billing_zones.getZone(zone_code)
                console.log("metroZone---@@@@@@@")
                if (consignor_zone_data != '') {
                    zone = consignor_zone_data[0].zone;
                    await zone_list.push({
                        zone: zone,
                    })
                } else {
                    zone_list = zone_list
                }
            }
        } else if (metroZoneData.length < 2 || metroZoneData == '') {
            zone_list = zone_list
        }

        // console.log("zonelist7---", zone_list)
        // Rest of India zone check
        var restOfIndiaZone = [{ zone_code: 'ROI' }];
        var zone_code7 = '';
        for (let czone of restOfIndiaZone) {
            zone_code7 = czone.zone_code
        }
        var restOfIndiaZoneData = await ilogix_standard_other_pincodes.getRestOfIndiaZone(zone_code7, deliveryPincode)
        if (restOfIndiaZoneData != '') {
            var restOfIndiaPincode = [];
            for (let cpincode of restOfIndiaZoneData) {
                restOfIndiaPincode.push(cpincode.pincode)
            }
            const found12 = restOfIndiaPincode.some(r => deliveryPincode.includes(r))
            if (found12 !== false) {
                zone_code = zone_code7
                var consignor_zone_data = await consignor_standard_billing_zones.getZone(zone_code)
                console.log("restOfIndiaZone---@@@@@@@")
                if (consignor_zone_data != '') {
                    zone = consignor_zone_data[0].zone;
                    await zone_list.push({
                        zone: zone,
                    })
                } else {
                    zone_list = zone_list
                }
            }

        } else if (restOfIndiaZoneData.length < 2 || restOfIndiaZoneData == '') {
            zone_list = zone_list
        }
        // console.log("zonelistROI---", zone_list)
        var courier_code_list = ''
        var zonePriorityData = [];
        var serviceableCourier = []
        zonePriorityData = await commanController.zonePriority(req, zone_list)
        if (zonePriorityData == 'No Data Found') {
            console.log("zonePriorityData---",zonePriorityData)
            next();
        } else {
            // console.log("priorityRules---", zonePriorityData)
            for await (let element of zonePriorityData) {
                console.log(element.list)
                for await (let code of element.list) {
                    console.log("courier_code---", code.courier_code)
                    if (courier_code_list == '')
                        courier_code_list = "'" + code.courier_code + "'"
                    else
                        courier_code_list += ",'" + code.courier_code + "'"
                }
            }
            var tenant_id = trim(req.body.tenant_id)
            var platform = trim(req.body.platform)
            if (platform.toUpperCase() == 'V1') {
                if (courier_code_list != '') {
                    var websiteV1 = [];
                    websiteV1 = await systemDao.getWebsiteData(tenant_id)
                    console.log("websiteV1---", websiteV1)
                    if (websiteV1.length != 0) {
                        for await (let eleme of websiteV1) {
                            var database = eleme.uuid
                            console.log("database@@@@---", database)
                            console.log("courier_code_list---", courier_code_list)
                            var sql21 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
                            dbClient.getMysqlDBClient(database).then(async function (connection2) {
                                connection2.query(sql21, async (error, results1) => {
                                    if (error) {
                                        return res.status(400).json({
                                            message: 'Bad Request',
                                            error: error.message
                                        })
                                    } else {
                                        console.log("resultsCouriers1----", results1)
                                        // if (results1) {
                                        var sql22 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="-1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';

                                        connection2.query(sql22, async (error, results2) => {
                                            if (error) {
                                                return res.status(400).json({
                                                    message: 'Bad Request',
                                                    error: error.message
                                                })
                                            } else {
                                                
                                                serviceableCourier = await systemDao.getServiceableCourier(deliveryPincode, courier_code_list)
                                                console.log("resultsCouriers@1----", results1)
                                                console.log("resultsCouriers@2----", results2)
                                                console.log("mastersqldata@3--", serviceableCourier)
                                                for await (let data1 of results1) {
                                                    const isFound1 = serviceableCourier.some(data => {
                                                        console.log("results2.courier_code--", data.courier_id)
                                                        console.log("serviceableCourier.courier_code--", data1.courier_id)

                                                        if (data.courier_id === data1.courier_id) {
                                                            return true;
                                                        }
                                                    });
                                                    console.log("isFound2---", isFound1)
                                                    if (isFound1 === false) {
                                                        console.log("myid---", data1.courier_id)
                                                        serviceableCourier.push({
                                                            courier_id: data1.courier_id
                                                        })
                                                    }
                                                }

                                                for await (let data1 of results2) {
                                                    const isFound = serviceableCourier.some(data => {
                                                        // console.log("mysql.courier_code--",element.courier_code)
                                                        if (data.courier_id === data1.courier_id) {
                                                            return true;
                                                        }
                                                    });
                                                    console.log("isFound---", isFound)
                                                    if (isFound === true) {
                                                        let el = serviceableCourier.find(itm => itm.courier_id === data1.courier_id);
                                                        if (el)
                                                        serviceableCourier.splice(serviceableCourier.indexOf(el), 1);
                                                    }
                                                }
                                                console.log("serviceableCourier---", serviceableCourier)
                                                if (serviceableCourier != '') {
                                                    commanController.recommendedPriorityRule2(req, zonePriorityData, serviceableCourier).then(function (newdata2) {
                                                        res.json({
                                                            status: 200,
                                                            data: newdata2
                                                        })
                                                    })
                                                } else {
                                                    next();
                                                }
                                            }
                                        })
                                    }
                                })
                            })
                        }
                    } else {
                        next()
                    }
                }
            }else if(platform.toUpperCase() == 'V2'){
                console.log("reached on V2")
                var serviceableCourier = []
                var websiteV2 = []
                websiteV2 = await websites2Model.find({_id: tenant_id})
                if(websiteV2 != ''){
                    for await(let data of websiteV2){
                        if(data.reverse_enabled_couriers != ''){
                            for await(let element of data.reverse_enabled_couriers){
                                serviceableCourier.push({
                                    courier_id: element
                                  })
                            }
                            commanController.recommendedPriorityRule2(req,zonePriorityData, serviceableCourier).then(function (newdata2) {
                                res.json({
                                    status: 200,
                                    data: newdata2
                                })
                            })
                        }else{
                            next();
                        }
                    }
                }else{
                    next()
                }
               

            }
        }

    } else {
        return res.status(400).json({
            message: 'All field are required..'
        })
    }

}

/*
    This route used for get recommendation priority rules using zone + weight || zone || weight value.
    Zone are get by using pickupPincode and deliveryPincode, and its find out using ilogix_standard_pincodes_master, ilogix_standard_other_pincodes and then use consignor_standard_billing_zones table.
*/
exports.getRecommendationPriority = async (req, res, next) => {
    console.log("reached getRecommendationPriority api")
    if (req.body.pickupPincode && req.body.deliveryPincode && req.body.platform && (req.body.pickupPincode !== undefined || req.body.pickupPincode !== 'undefined' || req.body.pickupPincode !== '') && (req.body.deliveryPincode !== undefined || req.body.deliveryPincode !== 'undefined' || req.body.deliveryPincode !== '') && (req.body.platform !== undefined || req.body.platform !== 'undefined' || req.body.platform !== '')) {
        var pickupPincode = trim(req.body.pickupPincode)
        var deliveryPincode = trim(req.body.deliveryPincode)
        let zone = '';
        var pincode_zones_data = [];
        var pincode_master = await ilogix_standard_pincodes_master.getIlogixStandardPincodes(pickupPincode, deliveryPincode)
        if (pincode_master && (pincode_master != '' && pincode_master.length >= 2)) {
            // console.log("pincode_master--",pincode_master)
            var pickupCity = pincode_master.find(x => x.pincode === pickupPincode).city_id;
            //  console.log("pickupCity----",pickupCity)
            var deliveryCity = pincode_master.find(x => x.pincode === deliveryPincode).city_id;
            // console.log("deliveryCity---",deliveryCity)
            var pickupState = pincode_master.find(x => x.pincode === pickupPincode).state_id;
            //  console.log("pickupState----",pickupState)
            var deliveryState = pincode_master.find(x => x.pincode === deliveryPincode).state_id;
            if (pickupCity && deliveryCity && pickupCity.toLowerCase() === deliveryCity.toLowerCase() && pickupState.toLowerCase() === deliveryState.toLowerCase()) {
                zone_code = 'IC';
                var consignor_zone_data = await consignor_standard_billing_zones.getZone(zone_code)
                if (consignor_zone_data != '') {
                    console.log("Intra City @@@@", consignor_zone_data)
                    zone = consignor_zone_data[0].zone;
                    await pincode_zones_data.push({
                        zone: zone,
                    })
                    var courier_code_list = '';
                    var priorityRules = [];
                    var serviceableCourier = []
                    priorityRules = await commanController.recommendedPriorityRule(req, zone)
                    if (priorityRules != 'All fields are required....') {
                        if (priorityRules.length != 0) {
                            for await (let element of priorityRules) {
                                console.log("element.list --priority", element.list)
                                for await (let code of element.list) {
                                    console.log("courier_code---", code.courier_code)
                                    if (courier_code_list == '')
                                        courier_code_list = "'" + code.courier_code + "'"
                                    else
                                        courier_code_list += ",'" + code.courier_code + "'"
                                }
                            }
                        } else {
                            courier_code_list = '';
                        }
                        var tenant_id = trim(req.body.tenant_id)
                        var platform = trim(req.body.platform)
                        if (platform.toUpperCase() == 'V1') {
                            if (courier_code_list != '') {
                                var websiteV1 = [];
                                websiteV1 = await systemDao.getWebsiteData(tenant_id)
                                console.log("websiteV1---", websiteV1)
                                if (websiteV1.length != 0) {
                                    for await (let eleme of websiteV1) {
                                        var database = eleme.uuid
                                        console.log("database@@@@---", database)
                                        console.log("courier_code_list---", courier_code_list)
                                        var sql21 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
                                        dbClient.getMysqlDBClient(database).then(async function (connection2) {
                                            connection2.query(sql21, async (error, results1) => {
                                                if (error) {
                                                    return res.status(400).json({
                                                        message: 'Bad Request',
                                                        error: error.message
                                                    })
                                                } else {
                                                    console.log("resultsCouriers1----", results1)
                                                    // if (results1) {
                                                    var sql22 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="-1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';

                                                    connection2.query(sql22, async (error, results2) => {
                                                        if (error) {
                                                            return res.status(400).json({
                                                                message: 'Bad Request',
                                                                error: error.message
                                                            })
                                                        } else {
                                                            serviceableCourier = await systemDao.getServiceableCourier(deliveryPincode, courier_code_list)
                                                            console.log("resultsCouriers@1----", results1)
                                                            console.log("resultsCouriers@2----", results2)
                                                            console.log("mastersqldata@3--", serviceableCourier)
                                                            for await (let data1 of results1) {
                                                                const isFound1 = serviceableCourier.some(data => {
                                                                    console.log("results2.courier_code--", data.courier_id)
                                                                    console.log("serviceableCourier.courier_code--", data1.courier_id)

                                                                    if (data.courier_id === data1.courier_id) {
                                                                        return true;
                                                                    }
                                                                });
                                                                console.log("isFound2---", isFound1)
                                                                if (isFound1 === false) {
                                                                    console.log("myid---", data1.courier_id)
                                                                    serviceableCourier.push({
                                                                        courier_id: data1.courier_id
                                                                    })
                                                                }
                                                            }

                                                            for await (let data1 of results2) {
                                                                const isFound = serviceableCourier.some(data => {
                                                                    // console.log("mysql.courier_code--",element.courier_code)
                                                                    if (data.courier_id === data1.courier_id) {
                                                                        return true;
                                                                    }
                                                                });
                                                                console.log("isFound---", isFound)
                                                                if (isFound === true) {
                                                                    let el = serviceableCourier.find(itm => itm.courier_id === data1.courier_id);
                                                                    if (el)
                                                                        serviceableCourier.splice(serviceableCourier.indexOf(el), 1);
                                                                }
                                                            }
                                                            console.log("serviceableCourier---", serviceableCourier)
                                                            if (serviceableCourier != '') {
                                                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                                    res.json({
                                                                        status: 200,
                                                                        data: newdata2
                                                                    })
                                                                })
                                                            } else {
                                                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                                    res.json({
                                                                        status: 200,
                                                                        data: newdata2
                                                                    })
                                                                })
                                                            }
                                                        }
                                                    })
                                                }
                                            })
                                        })
                                    }
                                } else {
                                    commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                        res.json({
                                            status: 200,
                                            data: newdata2
                                        })
                                    })
                                }
                            } else {
                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                    res.json({
                                        status: 200,
                                        data: newdata2
                                    })
                                })
                            }
                        } else if (platform.toUpperCase() == 'V2') {
                            console.log("reached on V2")
                            var serviceableCourier = []
                            var websiteV2 = []
                            websiteV2 = await websites2Model.find({ _id: tenant_id })
                            if (websiteV2 != '') {
                                for await (let data of websiteV2) {
                                    if (data.reverse_enabled_couriers != '') {
                                        for await (let element of data.reverse_enabled_couriers) {
                                            serviceableCourier.push({
                                                courier_id: element
                                            })
                                        }
                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                            res.json({
                                                status: 200,
                                                data: newdata2
                                            })
                                        })
                                    } else {
                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                            res.json({
                                                status: 200,
                                                data: newdata2
                                            })
                                        })
                                    }
                                }
                            } else {
                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                    res.json({
                                        status: 200,
                                        data: newdata2
                                    })
                                })
                            }


                        }
                    } else {
                        res.json({
                            status: 400,
                            data: 'All fields are required....'
                        })
                    }
                } else {
                    zone = ''
                    pincode_zones_data = pincode_zones_data
                }
            } else if (pickupCity && deliveryCity && pickupCity.toLowerCase() != deliveryCity.toLowerCase() && pickupState.toLowerCase() === deliveryState.toLowerCase()) {
                zone_code = 'IS';
                var consignor_zone_data = await consignor_standard_billing_zones.getZone(zone_code)
                if (consignor_zone_data != '') {
                    zone = consignor_zone_data[0].zone;
                    await pincode_zones_data.push({
                        zone: zone,
                    })
                    var courier_code_list = ''
                    var priorityRules = [];
                    var serviceableCourier = [];
                    priorityRules = await commanController.recommendedPriorityRule(req, zone)
                    // console.log("priorityRules @@@ --", priorityRules)
                    if (priorityRules != 'All fields are required....') {
                        if (priorityRules != '') {
                            for await (let element of priorityRules) {
                                console.log("element.list --priority", element.list)
                                for await (let code of element.list) {
                                    console.log("courier_code---", code.courier_code)
                                    if (courier_code_list == '')
                                        courier_code_list = "'" + code.courier_code + "'"
                                    else
                                        courier_code_list += ",'" + code.courier_code + "'"
                                }
                            }
                        } else {
                            courier_code_list = '';
                        }
                        var tenant_id = trim(req.body.tenant_id)
                        var platform = trim(req.body.platform)
                        console.log("platform.toUpperCase()--", platform.toUpperCase())
                        if (platform.toUpperCase() == 'V1') {
                            if (courier_code_list != '') {
                                var websiteV1 = [];
                                websiteV1 = await systemDao.getWebsiteData(tenant_id)
                                console.log("websiteV1---", websiteV1)
                                if (websiteV1.length != 0) {
                                    for await (let eleme of websiteV1) {
                                        var database = eleme.uuid
                                        console.log("database@@@@---", database)
                                        console.log("courier_code_list---", courier_code_list)
                                        var sql21 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
                                        dbClient.getMysqlDBClient(database).then(async function (connection2) {
                                            connection2.query(sql21, async (error, results1) => {
                                                if (error) {
                                                    return res.status(400).json({
                                                        message: 'Bad Request',
                                                        error: error.message
                                                    })
                                                } else {
                                                    console.log("resultsCouriers1----", results1)
                                                    // if (results1) {
                                                    var sql22 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="-1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';

                                                    connection2.query(sql22, async (error, results2) => {
                                                        if (error) {
                                                            return res.status(400).json({
                                                                message: 'Bad Request',
                                                                error: error.message
                                                            })
                                                        } else {

                                                            serviceableCourier = await systemDao.getServiceableCourier(deliveryPincode, courier_code_list)
                                                            console.log("resultsCouriers@1----", results1)
                                                            console.log("resultsCouriers@2----", results2)
                                                            console.log("mastersqldata@3--", serviceableCourier)
                                                            for await (let data1 of results1) {
                                                                const isFound1 = serviceableCourier.some(data => {
                                                                    console.log("results2.courier_code--", data.courier_id)
                                                                    console.log("serviceableCourier.courier_code--", data1.courier_id)

                                                                    if (data.courier_id === data1.courier_id) {
                                                                        return true;
                                                                    }
                                                                });
                                                                console.log("isFound2---", isFound1)
                                                                if (isFound1 === false) {
                                                                    console.log("myid---", data1.courier_id)
                                                                    serviceableCourier.push({
                                                                        courier_id: data1.courier_id
                                                                    })
                                                                }
                                                            }

                                                            for await (let data1 of results2) {
                                                                const isFound = serviceableCourier.some(data => {
                                                                    // console.log("mysql.courier_code--",element.courier_code)
                                                                    if (data.courier_id === data1.courier_id) {
                                                                        return true;
                                                                    }
                                                                });
                                                                console.log("isFound---", isFound)
                                                                if (isFound === true) {
                                                                    let el = serviceableCourier.find(itm => itm.courier_id === data1.courier_id);
                                                                    if (el)
                                                                        serviceableCourier.splice(serviceableCourier.indexOf(el), 1);
                                                                }
                                                            }
                                                            console.log("serviceableCourier---", serviceableCourier)
                                                            if (serviceableCourier != '') {
                                                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                                    res.json({
                                                                        status: 200,
                                                                        data: newdata2
                                                                    })
                                                                })
                                                            } else {
                                                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                                    res.json({
                                                                        status: 200,
                                                                        data: newdata2
                                                                    })
                                                                })
                                                            }
                                                        }
                                                    })
                                                }
                                            })
                                        })
                                    }
                                } else {
                                    commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                        res.json({
                                            status: 200,
                                            data: newdata2
                                        })
                                    })
                                }
                            } else {
                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                    res.json({
                                        status: 200,
                                        data: newdata2
                                    })
                                })
                            }
                        } else if (platform.toUpperCase() == 'V2') {
                            console.log("reached on V2")
                            var serviceableCourier = []
                            var websiteV2 = []
                            websiteV2 = await websites2Model.find({ _id: tenant_id })
                            if (websiteV2 != '') {
                                for await (let data of websiteV2) {
                                    if (data.reverse_enabled_couriers != '') {
                                        for await (let element of data.reverse_enabled_couriers) {
                                            serviceableCourier.push({
                                                courier_id: element
                                            })
                                        }
                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                            res.json({
                                                status: 200,
                                                data: newdata2
                                            })
                                        })
                                    } else {
                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                            res.json({
                                                status: 200,
                                                data: newdata2
                                            })
                                        })
                                    }
                                }
                            } else {
                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                    res.json({
                                        status: 200,
                                        data: newdata2
                                    })
                                })
                            }


                        }
                    } else {
                        res.json({
                            status: 400,
                            data: 'All fields are required....'
                        })
                    }

                } else {
                    pincode_zones_data = pincode_zones_data
                }
            }
        } else {
            pincode_zones_data = pincode_zones_data
        }
        console.log("zone---", zone)
        if (zone == '') {
            if (pincode_zones_data.length == 0) {
                // NJ Special Zone check
                console.log("nj zone checked.....")
                var zone_main = ['IC', 'IS'];
                var zone_code1 = '';
                const found = zone_main.some(r => zone.includes(r))
                if (zone == '' || found === false) {
                    var customList = await consignor_standard_billing_zones.customDeliveryList()
                    // console.log("customList---",customList)
                    for (let czone of customList) {
                        zone_code1 = czone.zone_code;
                    }
                    var otherPincodeZone = await ilogix_standard_other_pincodes.getOtherPincodeZone(zone_code1, deliveryPincode)
                    // console.log("otherPincodeZone---",otherPincodeZone)
                    if (otherPincodeZone != '') {
                        var custom_pickup_pincodes = [];
                        for (let cpincode of otherPincodeZone) {
                            custom_pickup_pincodes.push(cpincode.pincode)
                        }
                        const found1 = custom_pickup_pincodes.some(r => deliveryPincode.includes(r))
                        if (found1 !== false) {
                            console.log("otherPincodeZone---@@@@@@@")
                            zone_code = zone_code1;
                            var consignor_zone_data = await consignor_standard_billing_zones.getZone(zone_code)
                            if (consignor_zone_data != '') {
                                zone = consignor_zone_data[0].zone;
                                await pincode_zones_data.push({
                                    zone: zone,
                                })
                                var courier_code_list = ''
                                var priorityRules = [];
                                var serviceableCourier = []
                                priorityRules = await commanController.recommendedPriorityRule(req, zone)
                                if (priorityRules != 'All fields are required....') {
                                    if (priorityRules.length != 0) {
                                        for await (let element of priorityRules) {
                                            console.log("element.list --priority", element.list)
                                            for await (let code of element.list) {
                                                console.log("courier_code---", code.courier_code)
                                                if (courier_code_list == '')
                                                    courier_code_list = "'" + code.courier_code + "'"
                                                else
                                                    courier_code_list += ",'" + code.courier_code + "'"
                                            }
                                        }
                                    } else {
                                        courier_code_list = '';
                                    }
                                    var tenant_id = trim(req.body.tenant_id)
                                    var platform = trim(req.body.platform)
                                    if (platform.toUpperCase() == 'V1') {
                                        if (courier_code_list != '') {
                                            var websiteV1 = [];
                                            websiteV1 = await systemDao.getWebsiteData(tenant_id)
                                            console.log("websiteV1---", websiteV1)
                                            if (websiteV1.length != 0) {
                                                for await (let eleme of websiteV1) {
                                                    var database = eleme.uuid
                                                    console.log("database@@@@---", database)
                                                    console.log("courier_code_list---", courier_code_list)
                                                    var sql21 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
                                                    dbClient.getMysqlDBClient(database).then(async function (connection2) {
                                                        connection2.query(sql21, async (error, results1) => {
                                                            if (error) {
                                                                return res.status(400).json({
                                                                    message: 'Bad Request',
                                                                    error: error.message
                                                                })
                                                            } else {
                                                                console.log("resultsCouriers1----", results1)
                                                                // if (results1) {
                                                                var sql22 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="-1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';

                                                                connection2.query(sql22, async (error, results2) => {
                                                                    if (error) {
                                                                        return res.status(400).json({
                                                                            message: 'Bad Request',
                                                                            error: error.message
                                                                        })
                                                                    } else {

                                                                        serviceableCourier = await systemDao.getServiceableCourier(deliveryPincode, courier_code_list)
                                                                        console.log("resultsCouriers@1----", results1)
                                                                        console.log("resultsCouriers@2----", results2)
                                                                        console.log("mastersqldata@3--", serviceableCourier)
                                                                        for await (let data1 of results1) {
                                                                            const isFound1 = serviceableCourier.some(data => {
                                                                                console.log("results2.courier_code--", data.courier_id)
                                                                                console.log("serviceableCourier.courier_code--", data1.courier_id)

                                                                                if (data.courier_id === data1.courier_id) {
                                                                                    return true;
                                                                                }
                                                                            });
                                                                            console.log("isFound2---", isFound1)
                                                                            if (isFound1 === false) {
                                                                                console.log("myid---", data1.courier_id)
                                                                                serviceableCourier.push({
                                                                                    courier_id: data1.courier_id
                                                                                })
                                                                            }
                                                                        }

                                                                        for await (let data1 of results2) {
                                                                            const isFound = serviceableCourier.some(data => {
                                                                                // console.log("mysql.courier_code--",element.courier_code)
                                                                                if (data.courier_id === data1.courier_id) {
                                                                                    return true;
                                                                                }
                                                                            });
                                                                            console.log("isFound---", isFound)
                                                                            if (isFound === true) {
                                                                                let el = serviceableCourier.find(itm => itm.courier_id === data1.courier_id);
                                                                                if (el)
                                                                                    serviceableCourier.splice(serviceableCourier.indexOf(el), 1);
                                                                            }
                                                                        }
                                                                        console.log("serviceableCourier---", serviceableCourier)
                                                                        if (serviceableCourier != '') {
                                                                            commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                                                res.json({
                                                                                    status: 200,
                                                                                    data: newdata2
                                                                                })
                                                                            })
                                                                        } else {
                                                                            commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                                                res.json({
                                                                                    status: 200,
                                                                                    data: newdata2
                                                                                })
                                                                            })
                                                                        }
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    })
                                                }
                                            } else {
                                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                    res.json({
                                                        status: 200,
                                                        data: newdata2
                                                    })
                                                })
                                            }
                                        }
                                    } else if (platform.toUpperCase() == 'V2') {
                                        console.log("reached on V2")
                                        var serviceableCourier = []
                                        var websiteV2 = []
                                        websiteV2 = await websites2Model.find({ _id: tenant_id })
                                        if (websiteV2 != '') {
                                            for await (let data of websiteV2) {
                                                if (data.reverse_enabled_couriers != '') {
                                                    for await (let element of data.reverse_enabled_couriers) {
                                                        serviceableCourier.push({
                                                            courier_id: element
                                                        })
                                                    }
                                                    commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                        res.json({
                                                            status: 200,
                                                            data: newdata2
                                                        })
                                                    })
                                                } else {
                                                    commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                        res.json({
                                                            status: 200,
                                                            data: newdata2
                                                        })
                                                    })
                                                }
                                            }
                                        } else {
                                            commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                res.json({
                                                    status: 200,
                                                    data: newdata2
                                                })
                                            })
                                        }


                                    }
                                } else {
                                    res.json({
                                        status: 400,
                                        data: 'All fields are required....'
                                    })
                                }

                            } else {
                                pincode_zones_data = pincode_zones_data
                            }
                        } else {
                            pincode_zones_data = pincode_zones_data
                        }
                    } else {
                        pincode_zones_data = pincode_zones_data
                    }


                }
            }

            if (pincode_zones_data.length == 0) {
                // West zone check
                var westZone = [{ zone_code: 'WZ' }];
                var zone_code2 = '';
                for (let czone of westZone) {
                    zone_code2 = czone.zone_code
                }
                var westZoneData = await ilogix_standard_other_pincodes.getOtherPincodeZone2(zone_code2, pickupPincode, deliveryPincode)
                if (westZoneData != '') {
                    // console.log("reached on west zone @@@@@@--",westZoneData)
                    var westPincode = [];
                    for (let cpincode of westZoneData) {
                        westPincode.push(cpincode.pincode)
                    }
                    const found2 = westPincode.some(r => pickupPincode.includes(r))
                    const found3 = westPincode.some(r => deliveryPincode.includes(r))
                    if (found2 !== false && found3 !== false) {
                        zone_code = zone_code2
                        console.log("westZone---@@@@@@@")
                        var consignor_zone_data = await consignor_standard_billing_zones.getZone(zone_code)
                        if (consignor_zone_data != '') {
                            zone = consignor_zone_data[0].zone;
                            await pincode_zones_data.push({
                                zone: zone,
                            })
                            var courier_code_list = '';
                            var priorityRules = [];
                            var serviceableCourier = []
                            priorityRules = await commanController.recommendedPriorityRule(req, zone)
                            console.log("priorityRules---", priorityRules)
                            if (priorityRules != 'All fields are required....') {
                                if (priorityRules.length != 0) {
                                    for await (let element of priorityRules) {
                                        console.log("element.list --priority", element.list)
                                        for await (let code of element.list) {
                                            console.log("courier_code---", code.courier_code)
                                            if (courier_code_list == '')
                                                courier_code_list = "'" + code.courier_code + "'"
                                            else
                                                courier_code_list += ",'" + code.courier_code + "'"
                                        }
                                    }
                                } else {
                                    courier_code_list = '';
                                }
                                var tenant_id = trim(req.body.tenant_id)
                                var platform = trim(req.body.platform)
                                if (platform.toUpperCase() == 'V1') {
                                    if (courier_code_list != '') {
                                        var websiteV1 = [];
                                        websiteV1 = await systemDao.getWebsiteData(tenant_id)
                                        console.log("websiteV1---", websiteV1)
                                        if (websiteV1.length != 0) {
                                            for await (let eleme of websiteV1) {
                                                var database = eleme.uuid
                                                console.log("database@@@@---", database)
                                                console.log("courier_code_list---", courier_code_list)
                                                var sql21 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
                                                dbClient.getMysqlDBClient(database).then(async function (connection2) {
                                                    connection2.query(sql21, async (error, results1) => {
                                                        if (error) {
                                                            return res.status(400).json({
                                                                message: 'Bad Request',
                                                                error: error.message
                                                            })
                                                        } else {
                                                            console.log("resultsCouriers1----", results1)
                                                            // if (results1) {
                                                            var sql22 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="-1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';

                                                            connection2.query(sql22, async (error, results2) => {
                                                                if (error) {
                                                                    return res.status(400).json({
                                                                        message: 'Bad Request',
                                                                        error: error.message
                                                                    })
                                                                } else {

                                                                    serviceableCourier = await systemDao.getServiceableCourier(deliveryPincode, courier_code_list)
                                                                    console.log("resultsCouriers@1----", results1)
                                                                    console.log("resultsCouriers@2----", results2)
                                                                    console.log("mastersqldata@3--", serviceableCourier)
                                                                    for await (let data1 of results1) {
                                                                        const isFound1 = serviceableCourier.some(data => {
                                                                            console.log("results2.courier_code--", data.courier_id)
                                                                            console.log("serviceableCourier.courier_code--", data1.courier_id)

                                                                            if (data.courier_id === data1.courier_id) {
                                                                                return true;
                                                                            }
                                                                        });
                                                                        console.log("isFound2---", isFound1)
                                                                        if (isFound1 === false) {
                                                                            console.log("myid---", data1.courier_id)
                                                                            serviceableCourier.push({
                                                                                courier_id: data1.courier_id
                                                                            })
                                                                        }
                                                                    }

                                                                    for await (let data1 of results2) {
                                                                        const isFound = serviceableCourier.some(data => {
                                                                            // console.log("mysql.courier_code--",element.courier_code)
                                                                            if (data.courier_id === data1.courier_id) {
                                                                                return true;
                                                                            }
                                                                        });
                                                                        console.log("isFound---", isFound)
                                                                        if (isFound === true) {
                                                                            let el = serviceableCourier.find(itm => itm.courier_id === data1.courier_id);
                                                                            if (el)
                                                                                serviceableCourier.splice(serviceableCourier.indexOf(el), 1);
                                                                        }
                                                                    }
                                                                    console.log("serviceableCourier---", serviceableCourier)
                                                                    if (serviceableCourier != '') {
                                                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                                            res.json({
                                                                                status: 200,
                                                                                data: newdata2
                                                                            })
                                                                        })
                                                                    } else {
                                                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                                            res.json({
                                                                                status: 200,
                                                                                data: newdata2
                                                                            })
                                                                        })
                                                                    }
                                                                }
                                                            })
                                                        }
                                                    })
                                                })
                                            }
                                        } else {
                                            commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                res.json({
                                                    status: 200,
                                                    data: newdata2
                                                })
                                            })
                                        }
                                    } else {
                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                            res.json({
                                                status: 200,
                                                data: newdata2
                                            })
                                        })
                                    }
                                } else if (platform.toUpperCase() == 'V2') {
                                    console.log("reached on V2")
                                    var serviceableCourier = []
                                    var websiteV2 = []
                                    websiteV2 = await websites2Model.find({ _id: tenant_id })
                                    if (websiteV2 != '') {
                                        for await (let data of websiteV2) {
                                            if (data.reverse_enabled_couriers != '') {
                                                for await (let element of data.reverse_enabled_couriers) {
                                                    serviceableCourier.push({
                                                        courier_id: element
                                                    })
                                                }
                                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                    res.json({
                                                        status: 200,
                                                        data: newdata2
                                                    })
                                                })
                                            } else {
                                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                    res.json({
                                                        status: 200,
                                                        data: newdata2
                                                    })
                                                })
                                            }
                                        }
                                    } else {
                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                            res.json({
                                                status: 200,
                                                data: newdata2
                                            })
                                        })
                                    }


                                }
                            } else {
                                res.json({
                                    status: 400,
                                    data: 'All fields are required....'
                                })
                            }
                        } else {
                            pincode_zones_data = pincode_zones_data
                        }
                    } else {
                        pincode_zones_data = pincode_zones_data
                    }

                } else if (westZoneData.length < 2 || westZoneData == '') {
                    pincode_zones_data = pincode_zones_data
                }

            }

            if (pincode_zones_data.length == 0) {
                // East zone check
                var eastZone = [{ zone_code: 'EZ' }]
                var zone_code3 = '';
                for (let czone of eastZone) {
                    zone_code3 = czone.zone_code
                }
                var eastZoneData = await ilogix_standard_other_pincodes.getOtherPincodeZone2(zone_code3, pickupPincode, deliveryPincode)
                if (eastZoneData != '') {
                    var eastPincode = [];
                    for (let cpincode of eastZoneData) {
                        eastPincode.push(cpincode.pincode)
                    }
                    const found4 = eastPincode.some(r => pickupPincode.includes(r))
                    const found5 = eastPincode.some(r => deliveryPincode.includes(r))
                    if (found4 !== false && found5 !== false) {
                        zone_code = zone_code3;
                        console.log("eastzone---@@@@@@@")
                        var consignor_zone_data = await consignor_standard_billing_zones.getZone(zone_code)
                        if (consignor_zone_data != '') {
                            zone = consignor_zone_data[0].zone;
                            await pincode_zones_data.push({
                                zone: zone,
                            })
                            var courier_code_list = '';
                            var priorityRules = [];
                            var serviceableCourier = [];
                            priorityRules = await commanController.recommendedPriorityRule(req, zone)
                            if (priorityRules != 'All fields are required....') {
                                if (priorityRules.length != 0) {
                                    for await (let element of priorityRules) {
                                        console.log("element.list --priority", element.list)
                                        for await (let code of element.list) {
                                            console.log("courier_code---", code.courier_code)
                                            if (courier_code_list == '')
                                                courier_code_list = "'" + code.courier_code + "'"
                                            else
                                                courier_code_list += ",'" + code.courier_code + "'"
                                        }
                                    }
                                } else {
                                    courier_code_list = '';
                                }
                                var tenant_id = trim(req.body.tenant_id)
                                var platform = trim(req.body.platform)
                                if (platform.toUpperCase() == 'V1') {
                                    if (courier_code_list != '') {
                                        var websiteV1 = [];
                                        websiteV1 = await systemDao.getWebsiteData(tenant_id)
                                        console.log("websiteV1---", websiteV1)
                                        if (websiteV1.length != 0) {
                                            for await (let eleme of websiteV1) {
                                                var database = eleme.uuid
                                                console.log("database@@@@---", database)
                                                console.log("courier_code_list---", courier_code_list)
                                                var sql21 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
                                                dbClient.getMysqlDBClient(database).then(async function (connection2) {
                                                    connection2.query(sql21, async (error, results1) => {
                                                        if (error) {
                                                            return res.status(400).json({
                                                                message: 'Bad Request',
                                                                error: error.message
                                                            })
                                                        } else {
                                                            console.log("resultsCouriers1----", results1)
                                                            // if (results1) {
                                                            var sql22 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="-1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';

                                                            connection2.query(sql22, async (error, results2) => {
                                                                if (error) {
                                                                    return res.status(400).json({
                                                                        message: 'Bad Request',
                                                                        error: error.message
                                                                    })
                                                                } else {

                                                                    serviceableCourier = await systemDao.getServiceableCourier(deliveryPincode, courier_code_list)
                                                                    console.log("resultsCouriers@1----", results1)
                                                                    console.log("resultsCouriers@2----", results2)
                                                                    console.log("mastersqldata@3--", serviceableCourier)
                                                                    for await (let data1 of results1) {
                                                                        const isFound1 = serviceableCourier.some(data => {
                                                                            console.log("results2.courier_code--", data.courier_id)
                                                                            console.log("serviceableCourier.courier_code--", data1.courier_id)

                                                                            if (data.courier_id === data1.courier_id) {
                                                                                return true;
                                                                            }
                                                                        });
                                                                        console.log("isFound2---", isFound1)
                                                                        if (isFound1 === false) {
                                                                            console.log("myid---", data1.courier_id)
                                                                            serviceableCourier.push({
                                                                                courier_id: data1.courier_id
                                                                            })
                                                                        }
                                                                    }

                                                                    for await (let data1 of results2) {
                                                                        const isFound = serviceableCourier.some(data => {
                                                                            // console.log("mysql.courier_code--",element.courier_code)
                                                                            if (data.courier_id === data1.courier_id) {
                                                                                return true;
                                                                            }
                                                                        });
                                                                        console.log("isFound---", isFound)
                                                                        if (isFound === true) {
                                                                            let el = serviceableCourier.find(itm => itm.courier_id === data1.courier_id);
                                                                            if (el)
                                                                                serviceableCourier.splice(serviceableCourier.indexOf(el), 1);
                                                                        }
                                                                    }
                                                                    console.log("serviceableCourier---", serviceableCourier)
                                                                    if (serviceableCourier != '') {
                                                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                                            res.json({
                                                                                status: 200,
                                                                                data: newdata2
                                                                            })
                                                                        })
                                                                    } else {
                                                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                                            res.json({
                                                                                status: 200,
                                                                                data: newdata2
                                                                            })
                                                                        })
                                                                    }
                                                                }
                                                            })
                                                        }
                                                    })
                                                })
                                            }
                                        } else {
                                            commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                res.json({
                                                    status: 200,
                                                    data: newdata2
                                                })
                                            })
                                        }
                                    } else {
                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                            res.json({
                                                status: 200,
                                                data: newdata2
                                            })
                                        })
                                    }
                                } else if (platform.toUpperCase() == 'V2') {
                                    console.log("reached on V2")
                                    var serviceableCourier = []
                                    var websiteV2 = []
                                    websiteV2 = await websites2Model.find({ _id: tenant_id })
                                    if (websiteV2 != '') {
                                        for await (let data of websiteV2) {
                                            if (data.reverse_enabled_couriers != '') {
                                                for await (let element of data.reverse_enabled_couriers) {
                                                    serviceableCourier.push({
                                                        courier_id: element
                                                    })
                                                }
                                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                    res.json({
                                                        status: 200,
                                                        data: newdata2
                                                    })
                                                })
                                            } else {
                                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                    res.json({
                                                        status: 200,
                                                        data: newdata2
                                                    })
                                                })
                                            }
                                        }
                                    } else {
                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                            res.json({
                                                status: 200,
                                                data: newdata2
                                            })
                                        })
                                    }


                                }
                            } else {
                                res.json({
                                    status: 400,
                                    data: 'All fields are required....'
                                })
                            }

                        } else {
                            pincode_zones_data = pincode_zones_data
                        }
                    } else {
                        pincode_zones_data = pincode_zones_data
                    }
                } else if (eastZoneData.length < 2 || eastZoneData == '') {
                    pincode_zones_data = pincode_zones_data
                }
            }

            if (pincode_zones_data.length == 0) {
                // North zone check
                var northZone = [{ zone_code: 'NZ' }];
                var zone_code4 = ''
                for (let czone of northZone) {
                    zone_code4 = czone.zone_code
                }
                var northZoneData = await ilogix_standard_other_pincodes.getOtherPincodeZone2(zone_code4, pickupPincode, deliveryPincode)
                if (northZoneData != '') {
                    var northPincode = [];
                    for (let cpincode of northZoneData) {
                        northPincode.push(cpincode.pincode)
                    }
                    const found6 = northPincode.some(r => deliveryPincode.includes(r))
                    const found7 = northPincode.some(r => deliveryPincode.includes(r))
                    if (found6 !== false && found7 !== false) {
                        zone_code = zone_code4
                        console.log("northZone---@@@@@@@")
                        var consignor_zone_data = await consignor_standard_billing_zones.getZone(zone_code)
                        if (consignor_zone_data != '') {
                            zone = consignor_zone_data[0].zone;
                            await pincode_zones_data.push({
                                zone: zone,
                            })
                            var courier_code_list = '';
                            var priorityRules = [];
                            var serviceableCourier = [];
                            priorityRules = await commanController.recommendedPriorityRule(req, zone)
                            if (priorityRules != 'All fields are required....') {
                                if (priorityRules.length != 0) {
                                    for await (let element of priorityRules) {
                                        console.log("element.list --priority", element.list)
                                        for await (let code of element.list) {
                                            console.log("courier_code---", code.courier_code)
                                            if (courier_code_list == '')
                                                courier_code_list = "'" + code.courier_code + "'"
                                            else
                                                courier_code_list += ",'" + code.courier_code + "'"
                                        }
                                    }
                                } else {
                                    courier_code_list = '';
                                }
                                var tenant_id = trim(req.body.tenant_id)
                                var platform = trim(req.body.platform)
                                if (platform.toUpperCase() == 'V1') {
                                    if (courier_code_list != '') {
                                        var websiteV1 = [];
                                        websiteV1 = await systemDao.getWebsiteData(tenant_id)
                                        console.log("websiteV1---", websiteV1)
                                        if (websiteV1.length != 0) {
                                            for await (let eleme of websiteV1) {
                                                var database = eleme.uuid
                                                console.log("database@@@@---", database)
                                                console.log("courier_code_list---", courier_code_list)
                                                var sql21 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
                                                dbClient.getMysqlDBClient(database).then(async function (connection2) {
                                                    connection2.query(sql21, async (error, results1) => {
                                                        if (error) {
                                                            return res.status(400).json({
                                                                message: 'Bad Request',
                                                                error: error.message
                                                            })
                                                        } else {
                                                            console.log("resultsCouriers1----", results1)
                                                            // if (results1) {
                                                            var sql22 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="-1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';

                                                            connection2.query(sql22, async (error, results2) => {
                                                                if (error) {
                                                                    return res.status(400).json({
                                                                        message: 'Bad Request',
                                                                        error: error.message
                                                                    })
                                                                } else {

                                                                    serviceableCourier = await systemDao.getServiceableCourier(deliveryPincode, courier_code_list)
                                                                    console.log("resultsCouriers@1----", results1)
                                                                    console.log("resultsCouriers@2----", results2)
                                                                    console.log("mastersqldata@3--", serviceableCourier)
                                                                    for await (let data1 of results1) {
                                                                        const isFound1 = serviceableCourier.some(data => {
                                                                            console.log("results2.courier_code--", data.courier_id)
                                                                            console.log("serviceableCourier.courier_code--", data1.courier_id)

                                                                            if (data.courier_id === data1.courier_id) {
                                                                                return true;
                                                                            }
                                                                        });
                                                                        console.log("isFound2---", isFound1)
                                                                        if (isFound1 === false) {
                                                                            console.log("myid---", data1.courier_id)
                                                                            serviceableCourier.push({
                                                                                courier_id: data1.courier_id
                                                                            })
                                                                        }
                                                                    }

                                                                    for await (let data1 of results2) {
                                                                        const isFound = serviceableCourier.some(data => {
                                                                            // console.log("mysql.courier_code--",element.courier_code)
                                                                            if (data.courier_id === data1.courier_id) {
                                                                                return true;
                                                                            }
                                                                        });
                                                                        console.log("isFound---", isFound)
                                                                        if (isFound === true) {
                                                                            let el = serviceableCourier.find(itm => itm.courier_id === data1.courier_id);
                                                                            if (el)
                                                                                serviceableCourier.splice(serviceableCourier.indexOf(el), 1);
                                                                        }
                                                                    }
                                                                    console.log("serviceableCourier---", serviceableCourier)
                                                                    if (serviceableCourier != '') {
                                                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                                            res.json({
                                                                                status: 200,
                                                                                data: newdata2
                                                                            })
                                                                        })
                                                                    } else {
                                                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                                            res.json({
                                                                                status: 200,
                                                                                data: newdata2
                                                                            })
                                                                        })
                                                                    }
                                                                }
                                                            })
                                                        }
                                                    })
                                                })
                                            }
                                        } else {
                                            commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                res.json({
                                                    status: 200,
                                                    data: newdata2
                                                })
                                            })
                                        }
                                    } else {
                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                            res.json({
                                                status: 200,
                                                data: newdata2
                                            })
                                        })
                                    }
                                } else if (platform.toUpperCase() == 'V2') {
                                    console.log("reached on V2")
                                    var serviceableCourier = []
                                    var websiteV2 = []
                                    websiteV2 = await websites2Model.find({ _id: tenant_id })
                                    if (websiteV2 != '') {
                                        for await (let data of websiteV2) {
                                            if (data.reverse_enabled_couriers != '') {
                                                for await (let element of data.reverse_enabled_couriers) {
                                                    serviceableCourier.push({
                                                        courier_id: element
                                                    })
                                                }
                                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                    res.json({
                                                        status: 200,
                                                        data: newdata2
                                                    })
                                                })
                                            } else {
                                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                    res.json({
                                                        status: 200,
                                                        data: newdata2
                                                    })
                                                })
                                            }
                                        }
                                    } else {
                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                            res.json({
                                                status: 200,
                                                data: newdata2
                                            })
                                        })
                                    }


                                }
                            } else {
                                res.json({
                                    status: 400,
                                    data: 'All fields are required....'
                                })
                            }
                        } else {
                            pincode_zones_data = pincode_zones_data
                        }
                    }


                } else if (northZoneData.length < 2 || northZoneData == '') {
                    pincode_zones_data = pincode_zones_data
                }

            }

            if (pincode_zones_data.length == 0) {
                //South zone check
                var southZone = [{ zone_code: 'SZ' }];
                var zone_code5 = '';
                for (let czone of southZone) {
                    zone_code5 = czone.zone_code
                }
                var southZoneData = await ilogix_standard_other_pincodes.getOtherPincodeZone2(zone_code5, pickupPincode, deliveryPincode)
                if (southZoneData != '') {
                    var southPincode = [];
                    for (let cpincode of southZoneData) {
                        southPincode.push(cpincode.pincode)
                    }
                    const found8 = southPincode.some(r => pickupPincode.includes(r))
                    const found9 = southPincode.some(r => deliveryPincode.includes(r))
                    if (found8 !== false && found9 !== false) {
                        zone_code = zone_code5
                        console.log("southZone---@@@@@@@")
                        var consignor_zone_data = await consignor_standard_billing_zones.getZone(zone_code)
                        if (consignor_zone_data != '') {
                            zone = consignor_zone_data[0].zone;
                            await pincode_zones_data.push({
                                zone: zone,
                            })
                            var courier_code_list = '';
                            var priorityRules = [];
                            var serviceableCourier = [];
                            priorityRules = await commanController.recommendedPriorityRule(req, zone)
                            if (priorityRules != 'All fields are required....') {
                                if (priorityRules.length != 0) {
                                    for await (let element of priorityRules) {
                                        console.log("element.list --priority", element.list)
                                        for await (let code of element.list) {
                                            console.log("courier_code---", code.courier_code)
                                            if (courier_code_list == '')
                                                courier_code_list = "'" + code.courier_code + "'"
                                            else
                                                courier_code_list += ",'" + code.courier_code + "'"
                                        }
                                    }
                                } else {
                                    courier_code_list = '';
                                }
                                var tenant_id = trim(req.body.tenant_id)
                                var platform = trim(req.body.platform)
                                if (platform.toUpperCase() == 'V1') {
                                    if (courier_code_list != '') {
                                        var websiteV1 = [];
                                        websiteV1 = await systemDao.getWebsiteData(tenant_id)
                                        console.log("websiteV1---", websiteV1)
                                        if (websiteV1.length != 0) {
                                            for await (let eleme of websiteV1) {
                                                var database = eleme.uuid
                                                console.log("database@@@@---", database)
                                                console.log("courier_code_list---", courier_code_list)
                                                var sql21 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
                                                dbClient.getMysqlDBClient(database).then(async function (connection2) {
                                                    connection2.query(sql21, async (error, results1) => {
                                                        if (error) {
                                                            return res.status(400).json({
                                                                message: 'Bad Request',
                                                                error: error.message
                                                            })
                                                        } else {
                                                            console.log("resultsCouriers1----", results1)
                                                            // if (results1) {
                                                            var sql22 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="-1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';

                                                            connection2.query(sql22, async (error, results2) => {
                                                                if (error) {
                                                                    return res.status(400).json({
                                                                        message: 'Bad Request',
                                                                        error: error.message
                                                                    })
                                                                } else {

                                                                    serviceableCourier = await systemDao.getServiceableCourier(deliveryPincode, courier_code_list)
                                                                    console.log("resultsCouriers@1----", results1)
                                                                    console.log("resultsCouriers@2----", results2)
                                                                    console.log("mastersqldata@3--", serviceableCourier)
                                                                    for await (let data1 of results1) {
                                                                        const isFound1 = serviceableCourier.some(data => {
                                                                            console.log("results2.courier_code--", data.courier_id)
                                                                            console.log("serviceableCourier.courier_code--", data1.courier_id)

                                                                            if (data.courier_id === data1.courier_id) {
                                                                                return true;
                                                                            }
                                                                        });
                                                                        console.log("isFound2---", isFound1)
                                                                        if (isFound1 === false) {
                                                                            console.log("myid---", data1.courier_id)
                                                                            serviceableCourier.push({
                                                                                courier_id: data1.courier_id
                                                                            })
                                                                        }
                                                                    }

                                                                    for await (let data1 of results2) {
                                                                        const isFound = serviceableCourier.some(data => {
                                                                            // console.log("mysql.courier_code--",element.courier_code)
                                                                            if (data.courier_id === data1.courier_id) {
                                                                                return true;
                                                                            }
                                                                        });
                                                                        console.log("isFound---", isFound)
                                                                        if (isFound === true) {
                                                                            let el = serviceableCourier.find(itm => itm.courier_id === data1.courier_id);
                                                                            if (el)
                                                                                serviceableCourier.splice(serviceableCourier.indexOf(el), 1);
                                                                        }
                                                                    }
                                                                    console.log("serviceableCourier---", serviceableCourier)
                                                                    if (serviceableCourier != '') {
                                                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                                            res.json({
                                                                                status: 200,
                                                                                data: newdata2
                                                                            })
                                                                        })
                                                                    } else {
                                                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                                            res.json({
                                                                                status: 200,
                                                                                data: newdata2
                                                                            })
                                                                        })
                                                                    }
                                                                }
                                                            })
                                                        }
                                                    })
                                                })
                                            }
                                        } else {
                                            commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                res.json({
                                                    status: 200,
                                                    data: newdata2
                                                })
                                            })
                                        }
                                    } else {
                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                            res.json({
                                                status: 200,
                                                data: newdata2
                                            })
                                        })
                                    }
                                } else if (platform.toUpperCase() == 'V2') {
                                    console.log("reached on V2")
                                    var serviceableCourier = []
                                    var websiteV2 = []
                                    websiteV2 = await websites2Model.find({ _id: tenant_id })
                                    if (websiteV2 != '') {
                                        for await (let data of websiteV2) {
                                            if (data.reverse_enabled_couriers != '') {
                                                for await (let element of data.reverse_enabled_couriers) {
                                                    serviceableCourier.push({
                                                        courier_id: element
                                                    })
                                                }
                                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                    res.json({
                                                        status: 200,
                                                        data: newdata2
                                                    })
                                                })
                                            } else {
                                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                    res.json({
                                                        status: 200,
                                                        data: newdata2
                                                    })
                                                })
                                            }
                                        }
                                    } else {
                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                            res.json({
                                                status: 200,
                                                data: newdata2
                                            })
                                        })
                                    }


                                }
                            } else {
                                res.json({
                                    status: 400,
                                    data: 'All fields are required....'
                                })
                            }
                        } else {
                            pincode_zones_data = pincode_zones_data
                        }
                    } else {
                        pincode_zones_data = pincode_zones_data
                    }

                } else if (southZoneData.length < 2 || southZoneData == '') {
                    pincode_zones_data = pincode_zones_data
                }
            }

            if (pincode_zones_data.length == 0) {
                // Metro to Metro zone check
                var metroZone = [{ zone_code: 'MM' }];
                var zone_code6 = '';
                for (let czone of metroZone) {
                    zone_code6 = czone.zone_code
                }
                var metroZoneData = await ilogix_standard_other_pincodes.getMetroZone(zone_code6, pickupPincode, deliveryPincode)
                if (metroZoneData != '') {
                    var metroPincode = [];
                    for (let cpincode of metroZoneData) {
                        metroPincode.push(cpincode.pincode)
                    }
                    const found10 = metroPincode.some(r => pickupPincode.includes(r))
                    const found11 = metroPincode.some(r => deliveryPincode.includes(r))
                    if (found10 !== false && found11 !== false) {
                        zone_code = zone_code6
                        console.log("metroZone---@@@@@@@")
                        var consignor_zone_data = await consignor_standard_billing_zones.getZone(zone_code)
                        if (consignor_zone_data != '') {
                            zone = consignor_zone_data[0].zone;
                            await pincode_zones_data.push({
                                zone: zone,
                            })
                            var courier_code_list = '';
                            var priorityRules = [];
                            var serviceableCourier = []
                            priorityRules = await commanController.recommendedPriorityRule(req, zone)
                            if (priorityRules != 'All fields are required....') {
                                if (priorityRules.length != 0) {
                                    for await (let element of priorityRules) {
                                        console.log("element.list --priority", element.list)
                                        for await (let code of element.list) {
                                            console.log("courier_code---", code.courier_code)
                                            if (courier_code_list == '')
                                                courier_code_list = "'" + code.courier_code + "'"
                                            else
                                                courier_code_list += ",'" + code.courier_code + "'"
                                        }
                                    }
                                } else {
                                    courier_code_list = '';
                                }
                                var tenant_id = trim(req.body.tenant_id)
                                var platform = trim(req.body.platform)
                                if (platform.toUpperCase() == 'V1') {
                                    if (courier_code_list != '') {
                                        var websiteV1 = [];
                                        websiteV1 = await systemDao.getWebsiteData(tenant_id)
                                        console.log("websiteV1---", websiteV1)
                                        if (websiteV1.length != 0) {
                                            for await (let eleme of websiteV1) {
                                                var database = eleme.uuid
                                                console.log("database@@@@---", database)
                                                console.log("courier_code_list---", courier_code_list)
                                                var sql21 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
                                                dbClient.getMysqlDBClient(database).then(async function (connection2) {
                                                    connection2.query(sql21, async (error, results1) => {
                                                        if (error) {
                                                            return res.status(400).json({
                                                                message: 'Bad Request',
                                                                error: error.message
                                                            })
                                                        } else {
                                                            console.log("resultsCouriers1----", results1)
                                                            // if (results1) {
                                                            var sql22 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="-1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';

                                                            connection2.query(sql22, async (error, results2) => {
                                                                if (error) {
                                                                    return res.status(400).json({
                                                                        message: 'Bad Request',
                                                                        error: error.message
                                                                    })
                                                                } else {

                                                                    serviceableCourier = await systemDao.getServiceableCourier(deliveryPincode, courier_code_list)
                                                                    console.log("resultsCouriers@1----", results1)
                                                                    console.log("resultsCouriers@2----", results2)
                                                                    console.log("mastersqldata@3--", serviceableCourier)
                                                                    for await (let data1 of results1) {
                                                                        const isFound1 = serviceableCourier.some(data => {
                                                                            console.log("results2.courier_code--", data.courier_id)
                                                                            console.log("serviceableCourier.courier_code--", data1.courier_id)

                                                                            if (data.courier_id === data1.courier_id) {
                                                                                return true;
                                                                            }
                                                                        });
                                                                        console.log("isFound2---", isFound1)
                                                                        if (isFound1 === false) {
                                                                            console.log("myid---", data1.courier_id)
                                                                            serviceableCourier.push({
                                                                                courier_id: data1.courier_id
                                                                            })
                                                                        }
                                                                    }

                                                                    for await (let data1 of results2) {
                                                                        const isFound = serviceableCourier.some(data => {
                                                                            // console.log("mysql.courier_code--",element.courier_code)
                                                                            if (data.courier_id === data1.courier_id) {
                                                                                return true;
                                                                            }
                                                                        });
                                                                        console.log("isFound---", isFound)
                                                                        if (isFound === true) {
                                                                            let el = serviceableCourier.find(itm => itm.courier_id === data1.courier_id);
                                                                            if (el)
                                                                                serviceableCourier.splice(serviceableCourier.indexOf(el), 1);
                                                                        }
                                                                    }
                                                                    console.log("serviceableCourier---", serviceableCourier)
                                                                    if (serviceableCourier != '') {
                                                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                                            res.json({
                                                                                status: 200,
                                                                                data: newdata2
                                                                            })
                                                                        })
                                                                    } else {
                                                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                                            res.json({
                                                                                status: 200,
                                                                                data: newdata2
                                                                            })
                                                                        })
                                                                    }
                                                                }
                                                            })
                                                        }
                                                    })
                                                })
                                            }
                                        } else {
                                            commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                res.json({
                                                    status: 200,
                                                    data: newdata2
                                                })
                                            })
                                        }
                                    } else {
                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                            res.json({
                                                status: 200,
                                                data: newdata2
                                            })
                                        })
                                    }
                                } else if (platform.toUpperCase() == 'V2') {
                                    console.log("reached on V2")
                                    var serviceableCourier = []
                                    var websiteV2 = []
                                    websiteV2 = await websites2Model.find({ _id: tenant_id })
                                    if (websiteV2 != '') {
                                        for await (let data of websiteV2) {
                                            if (data.reverse_enabled_couriers != '') {
                                                for await (let element of data.reverse_enabled_couriers) {
                                                    serviceableCourier.push({
                                                        courier_id: element
                                                    })
                                                }
                                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                    res.json({
                                                        status: 200,
                                                        data: newdata2
                                                    })
                                                })
                                            } else {
                                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                    res.json({
                                                        status: 200,
                                                        data: newdata2
                                                    })
                                                })
                                            }
                                        }
                                    } else {
                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                            res.json({
                                                status: 200,
                                                data: newdata2
                                            })
                                        })
                                    }


                                }
                            } else {
                                res.json({
                                    status: 400,
                                    data: 'All fields are required....'
                                })
                            }
                        } else {
                            pincode_zones_data = pincode_zones_data
                        }
                    } else {
                        pincode_zones_data = pincode_zones_data
                    }
                } else if (metroZoneData.length < 2 || metroZoneData == '') {
                    pincode_zones_data = pincode_zones_data
                }
            }

            if (pincode_zones_data.length == 0) {
                // Rest of India zone check
                var restOfIndiaZone = [{ zone_code: 'ROI' }];
                var zone_code7 = '';
                for (let czone of restOfIndiaZone) {
                    zone_code7 = czone.zone_code
                }
                var restOfIndiaZoneData = await ilogix_standard_other_pincodes.getRestOfIndiaZone(zone_code7, deliveryPincode)
                if (restOfIndiaZoneData != '') {
                    var restOfIndiaPincode = [];
                    for (let cpincode of restOfIndiaZoneData) {
                        restOfIndiaPincode.push(cpincode.pincode)
                    }
                    const found12 = restOfIndiaPincode.some(r => deliveryPincode.includes(r))
                    if (found12 !== false) {
                        zone_code = zone_code7
                        console.log("restOfIndiaZone---@@@@@@@")
                        var consignor_zone_data = await consignor_standard_billing_zones.getZone(zone_code)
                        if (consignor_zone_data != '') {
                            zone = consignor_zone_data[0].zone;
                            await pincode_zones_data.push({
                                zone: zone,
                            })
                            var courier_code_list = '';
                            var priorityRules = [];
                            var serviceableCourier = [];
                            priorityRules = await commanController.recommendedPriorityRule(req, zone)
                            if (priorityRules != 'All fields are required....') {
                                if (priorityRules.length != 0) {
                                    for await (let element of priorityRules) {
                                        console.log("element.list --priority", element.list)
                                        for await (let code of element.list) {
                                            console.log("courier_code---", code.courier_code)
                                            if (courier_code_list == '')
                                                courier_code_list = "'" + code.courier_code + "'"
                                            else
                                                courier_code_list += ",'" + code.courier_code + "'"
                                        }
                                    }
                                } else {
                                    courier_code_list = '';
                                }
                                var tenant_id = trim(req.body.tenant_id)
                                var platform = trim(req.body.platform)
                                if (platform.toUpperCase() == 'V1') {
                                    if (courier_code_list != '') {
                                        var websiteV1 = [];
                                        websiteV1 = await systemDao.getWebsiteData(tenant_id)
                                        console.log("websiteV1---", websiteV1)
                                        if (websiteV1.length != 0) {
                                            for await (let eleme of websiteV1) {
                                                var database = eleme.uuid
                                                console.log("database@@@@---", database)
                                                console.log("courier_code_list---", courier_code_list)
                                                var sql21 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
                                                dbClient.getMysqlDBClient(database).then(async function (connection2) {
                                                    connection2.query(sql21, async (error, results1) => {
                                                        if (error) {
                                                            return res.status(400).json({
                                                                message: 'Bad Request',
                                                                error: error.message
                                                            })
                                                        } else {
                                                            console.log("resultsCouriers1----", results1)
                                                            // if (results1) {
                                                            var sql22 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="-1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';

                                                            connection2.query(sql22, async (error, results2) => {
                                                                if (error) {
                                                                    return res.status(400).json({
                                                                        message: 'Bad Request',
                                                                        error: error.message
                                                                    })
                                                                } else {

                                                                    serviceableCourier = await systemDao.getServiceableCourier(deliveryPincode, courier_code_list)
                                                                    console.log("resultsCouriers@1----", results1)
                                                                    console.log("resultsCouriers@2----", results2)
                                                                    console.log("mastersqldata@3--", serviceableCourier)
                                                                    for await (let data1 of results1) {
                                                                        const isFound1 = serviceableCourier.some(data => {
                                                                            console.log("results2.courier_code--", data.courier_id)
                                                                            console.log("serviceableCourier.courier_code--", data1.courier_id)

                                                                            if (data.courier_id === data1.courier_id) {
                                                                                return true;
                                                                            }
                                                                        });
                                                                        console.log("isFound2---", isFound1)
                                                                        if (isFound1 === false) {
                                                                            console.log("myid---", data1.courier_id)
                                                                            serviceableCourier.push({
                                                                                courier_id: data1.courier_id
                                                                            })
                                                                        }
                                                                    }

                                                                    for await (let data1 of results2) {
                                                                        const isFound = serviceableCourier.some(data => {
                                                                            // console.log("mysql.courier_code--",element.courier_code)
                                                                            if (data.courier_id === data1.courier_id) {
                                                                                return true;
                                                                            }
                                                                        });
                                                                        console.log("isFound---", isFound)
                                                                        if (isFound === true) {
                                                                            let el = serviceableCourier.find(itm => itm.courier_id === data1.courier_id);
                                                                            if (el)
                                                                                serviceableCourier.splice(serviceableCourier.indexOf(el), 1);
                                                                        }
                                                                    }
                                                                    console.log("serviceableCourier---", serviceableCourier)
                                                                    if (serviceableCourier != '') {
                                                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                                            res.json({
                                                                                status: 200,
                                                                                data: newdata2
                                                                            })
                                                                        })
                                                                    } else {
                                                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                                            res.json({
                                                                                status: 200,
                                                                                data: newdata2
                                                                            })
                                                                        })
                                                                    }
                                                                }
                                                            })
                                                        }
                                                    })
                                                })
                                            }
                                        } else {
                                            commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                res.json({
                                                    status: 200,
                                                    data: newdata2
                                                })
                                            })
                                        }
                                    } else {
                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                            res.json({
                                                status: 200,
                                                data: newdata2
                                            })
                                        })
                                    }
                                } else if (platform.toUpperCase() == 'V2') {
                                    console.log("reached on V2")
                                    var serviceableCourier = []
                                    var websiteV2 = []
                                    websiteV2 = await websites2Model.find({ _id: tenant_id })
                                    if (websiteV2 != '') {
                                        for await (let data of websiteV2) {
                                            if (data.reverse_enabled_couriers != '') {
                                                for await (let element of data.reverse_enabled_couriers) {
                                                    serviceableCourier.push({
                                                        courier_id: element
                                                    })
                                                }
                                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                    res.json({
                                                        status: 200,
                                                        data: newdata2
                                                    })
                                                })
                                            } else {
                                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                    res.json({
                                                        status: 200,
                                                        data: newdata2
                                                    })
                                                })
                                            }
                                        }
                                    } else {
                                        commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                            res.json({
                                                status: 200,
                                                data: newdata2
                                            })
                                        })
                                    }


                                }
                            } else {
                                res.json({
                                    status: 400,
                                    data: 'All fields are required....'
                                })
                            }
                        } else {
                            pincode_zones_data = pincode_zones_data
                        }
                    } else {
                        pincode_zones_data = pincode_zones_data
                    }

                } else if (restOfIndiaZoneData.length < 2 || restOfIndiaZoneData == '') {
                    pincode_zones_data = pincode_zones_data
                }
            }

            if (pincode_zones_data == 0) {
                zone = zone
                var courier_code_list = '';
                var priorityRules = [];
                var serviceableCourier = [];
                priorityRules = await commanController.recommendedPriorityRule(req, zone)
                if (priorityRules != 'All fields are required....') {
                    if (priorityRules.length != 0) {
                        for await (let element of priorityRules) {
                            console.log("element.list --priority", element.list)
                            for await (let code of element.list) {
                                console.log("courier_code---", code.courier_code)
                                if (courier_code_list == '')
                                    courier_code_list = "'" + code.courier_code + "'"
                                else
                                    courier_code_list += ",'" + code.courier_code + "'"
                            }
                        }
                    } else {
                        courier_code_list = '';
                    }
                    var tenant_id = trim(req.body.tenant_id)
                    var platform = trim(req.body.platform)
                    if (platform.toUpperCase() == 'V1') {
                        if (courier_code_list != '') {
                            var websiteV1 = [];
                            websiteV1 = await systemDao.getWebsiteData(tenant_id)
                            console.log("websiteV1---", websiteV1)
                            if (websiteV1.length != 0) {
                                for await (let eleme of websiteV1) {
                                    var database = eleme.uuid
                                    console.log("database@@@@---", database)
                                    console.log("courier_code_list---", courier_code_list)
                                    var sql21 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
                                    dbClient.getMysqlDBClient(database).then(async function (connection2) {
                                        connection2.query(sql21, async (error, results1) => {
                                            if (error) {
                                                return res.status(400).json({
                                                    message: 'Bad Request',
                                                    error: error.message
                                                })
                                            } else {
                                                console.log("resultsCouriers1----", results1)
                                                // if (results1) {
                                                var sql22 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="-1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';

                                                connection2.query(sql22, async (error, results2) => {
                                                    if (error) {
                                                        return res.status(400).json({
                                                            message: 'Bad Request',
                                                            error: error.message
                                                        })
                                                    } else {

                                                        serviceableCourier = await systemDao.getServiceableCourier(deliveryPincode, courier_code_list)
                                                        console.log("resultsCouriers@1----", results1)
                                                        console.log("resultsCouriers@2----", results2)
                                                        console.log("mastersqldata@3--", serviceableCourier)
                                                        for await (let data1 of results1) {
                                                            const isFound1 = serviceableCourier.some(data => {
                                                                console.log("results2.courier_code--", data.courier_id)
                                                                console.log("serviceableCourier.courier_code--", data1.courier_id)

                                                                if (data.courier_id === data1.courier_id) {
                                                                    return true;
                                                                }
                                                            });
                                                            console.log("isFound2---", isFound1)
                                                            if (isFound1 === false) {
                                                                console.log("myid---", data1.courier_id)
                                                                serviceableCourier.push({
                                                                    courier_id: data1.courier_id
                                                                })
                                                            }
                                                        }

                                                        for await (let data1 of results2) {
                                                            const isFound = serviceableCourier.some(data => {
                                                                // console.log("mysql.courier_code--",element.courier_code)
                                                                if (data.courier_id === data1.courier_id) {
                                                                    return true;
                                                                }
                                                            });
                                                            console.log("isFound---", isFound)
                                                            if (isFound === true) {
                                                                let el = serviceableCourier.find(itm => itm.courier_id === data1.courier_id);
                                                                if (el)
                                                                    serviceableCourier.splice(serviceableCourier.indexOf(el), 1);
                                                            }
                                                        }
                                                        console.log("serviceableCourier---", serviceableCourier)
                                                        if (serviceableCourier != '') {
                                                            commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                                res.json({
                                                                    status: 200,
                                                                    data: newdata2
                                                                })
                                                            })
                                                        } else {
                                                            commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                                                res.json({
                                                                    status: 200,
                                                                    data: newdata2
                                                                })
                                                            })
                                                        }
                                                    }
                                                })
                                            }
                                        })
                                    })
                                }
                            } else {
                                commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                    res.json({
                                        status: 200,
                                        data: newdata2
                                    })
                                })
                            }
                        } else {
                            commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                res.json({
                                    status: 200,
                                    data: newdata2
                                })
                            })
                        }
                    } else if (platform.toUpperCase() == 'V2') {
                        console.log("reached on V2")
                        var serviceableCourier = []
                        var websiteV2 = []
                        websiteV2 = await websites2Model.find({ _id: tenant_id })
                        if (websiteV2 != '') {
                            for await (let data of websiteV2) {
                                if (data.reverse_enabled_couriers != '') {
                                    for await (let element of data.reverse_enabled_couriers) {
                                        serviceableCourier.push({
                                            courier_id: element
                                        })
                                    }
                                    commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                        res.json({
                                            status: 200,
                                            data: newdata2
                                        })
                                    })
                                } else {
                                    commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                        res.json({
                                            status: 200,
                                            data: newdata2
                                        })
                                    })
                                }
                            }
                        } else {
                            commanController.recommendedPriorityRule2(req, priorityRules, serviceableCourier).then(function (newdata2) {
                                res.json({
                                    status: 200,
                                    data: newdata2
                                })
                            })
                        }


                    }
                } else {
                    res.json({
                        status: 400,
                        data: 'All fields are required....'
                    })
                }
            }
        }
    } else {
        return res.status(400).json({
            message: 'All field are required..'
        })
    }
}