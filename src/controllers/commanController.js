
const priorityRulesModel = require('../models/priority_rules');
var trim = require('trim');

exports.zonePriority = async (req, zone_list) => {
    var priorityRules = [];
    var priorityRules1 = [];
    console.log("req.body----", req.body)
    console.log("zone_list@@---", zone_list)
    if (req.body.platform && (req.body.platform !== undefined || req.body.platform !== 'undefined' || req.body.platform !== '')
        && req.body.pay_type && (req.body.pay_type !== undefined || req.body.pay_type !== 'undefined' || req.body.pay_type !== '')
        && req.body.tenant_id && (req.body.tenant_id !== undefined || req.body.tenant_id !== 'undefined' || req.body.tenant_id !== '')) {
        for (let data of zone_list) {
            if (priorityRules.length == 0) {
                var zone = data.zone
                // console.log("zone---",zone)
                if (zone && req.body.weight && (zone !== '' || zone !== undefined) && (req.body.weight !== undefined || req.body.weight !== 'undefined' || req.body.weight !== '')) {
                    var tenant_id = trim(req.body.tenant_id)
                    var pay_type = trim(req.body.pay_type)
                    var weight = trim(req.body.weight)
                    var zone = trim(zone)
                    var weight1 = parseFloat(weight)
                    console.log("weight1---", weight1)
                    if (pay_type) {
                        priorityRules1 = await priorityRulesModel.find({
                            tenant_id: tenant_id,
                            pay_type: pay_type.toUpperCase(),
                            zone: zone,
                            rule_code: 'WZ',
                            from: { "$lt": weight1 },
                            to: { "$gte": weight1 },
                        });
                        console.log("priorityRules1.length---", priorityRules1.length)
                        if (priorityRules1.length != 0) {
                            for (let data of priorityRules1) {
                                if (data.list != '') {
                                    priorityRules = priorityRules1
                                } else {
                                    priorityRules = []
                                }
                            }
                        } else {
                            priorityRules = []
                        }
                    }

                    if (priorityRules.length == 0) {
                        priorityRules1 = await priorityRulesModel.find({
                            tenant_id: tenant_id,
                            pay_type: 'BOTH',
                            rule_code: 'WZ',
                            zone: zone,
                            from: { "$lt": weight1 },
                            to: { "$gte": weight1 },
                        });
                        if (priorityRules1.length != 0) {
                            for (let data of priorityRules1) {
                                if (data.list != '') {
                                    priorityRules = priorityRules1
                                } else {
                                    priorityRules = []
                                }
                            }
                        } else {
                            priorityRules = []
                        }
                    }
                }
                if (priorityRules.length == 0) {
                    if (zone && (zone !== '' || zone !== undefined) && (req.body.weight === undefined || req.body.weight === 'undefined' || req.body.weight === '' || req.body.weight)) {
                        console.log("zone rule---ZM")
                        var tenant_id = trim(req.body.tenant_id)
                        var pay_type = trim(req.body.pay_type)
                        var zone = trim(zone)
                        if (pay_type) {
                            priorityRules1 = await priorityRulesModel.find({
                                tenant_id: tenant_id,
                                rule_code: 'ZM',
                                pay_type: pay_type.toUpperCase(),
                                zone: zone,
                            });
                            if (priorityRules1.length != 0) {
                                for (let data of priorityRules1) {
                                    if (data.list != '') {
                                        priorityRules = priorityRules1
                                    } else {
                                        priorityRules = []
                                    }
                                }
                            } else {
                                priorityRules = []
                            }
                        }
                        console.log("priorityRules---", priorityRules.length)
                        if (priorityRules.length == 0) {
                            console.log("ZM--req.body--", tenant_id, zone)
                            priorityRules1 = await priorityRulesModel.find({
                                tenant_id: tenant_id,
                                rule_code: 'ZM',
                                pay_type: 'BOTH',
                                zone: zone,
                            });
                            if (priorityRules1.length != 0) {
                                for (let data of priorityRules1) {
                                    if (data.list != '') {
                                        priorityRules = priorityRules1
                                    } else {
                                        priorityRules = []
                                    }
                                }
                            } else {
                                priorityRules = []
                            }
                        }

                    }
                }
            }
        }
        if (priorityRules1.length == 0) {
            priorityRules = 'No Data Found'
        }
    } else {
        priorityRules = 'All fields are required....'
    }
    return priorityRules
}

exports.recommendedPriorityRule2 = async (req, priorityRules, serviceablePincodes) => {
    console.log("req.body----", req.body)
    // console.log("priorityRules----", priorityRules)
    var newdata = []
    // console.log("serviceablePincodes---", serviceablePincodes)
    if (priorityRules.length != 0) {
        var newData2 = []
        // priorityRules.forEach(function (data) {
        for (let data of priorityRules) {
            if (data.list != '' && serviceablePincodes != '') {
                console.log("data.list---", data.list)
                for (var i = 0; i <= data.list.length - 1; i++) {
                    for (var j = 0; j < serviceablePincodes.length; j++) {
                        if (data.list[i] && (data.list[i].courier_code === serviceablePincodes[j].courier_id)) {

                            newdata.push({
                                courier_code: data.list[i].courier_code
                            })
                        }
                    }
                }
                console.log("newdata---array--", newdata)
                // priorityRules.forEach(function (data) {
                  if(newdata.length != 0){
                    for (let data of priorityRules) {
                      newData2.push({
                          _id: data._id,
                          tenant_id: data.tenant_id,
                          rule_code: data.rule_code,
                          list: newdata
                      })
                  }
                  }else{
                    var newData2 = []
                    console.log("length----", priorityRules.length)
                    var tenant_id = trim(req.body.tenant_id)
                    if (req.body.pay_type) {
                        var pay_type = trim(req.body.pay_type)
                        console.log("if part--", tenant_id, pay_type)
                        newData2 = await priorityRulesModel.find({
                            tenant_id: tenant_id,
                            pay_type: pay_type.toUpperCase(),
                            rule_code: 'default_rule',
                        });
            
                    }
                    console.log("newData2  --", newData2.length)
                    if (newData2.length == 0) {
                        console.log("else part--", tenant_id)
                        newData2 = await priorityRulesModel.find({
                            tenant_id: tenant_id,
                            pay_type: 'BOTH',
                            rule_code: 'default_rule',
                        });
                    }
                  }

                // })
            } else {
                console.log("@@@@@@@@@@@@@@@@@@")
                var newData2 = []
                console.log("length----", priorityRules.length)
                var tenant_id = trim(req.body.tenant_id)
                if (req.body.pay_type) {
                    var pay_type = trim(req.body.pay_type)
                    console.log("if part--", tenant_id, pay_type)
                    newData2 = await priorityRulesModel.find({
                        tenant_id: tenant_id,
                        pay_type: pay_type.toUpperCase(),
                        rule_code: 'default_rule',
                    });

                }
                console.log("newData2  --", newData2.length)
                if (newData2.length == 0 || newData2 == undefined) {
                    console.log("else part--", tenant_id)
                    newData2 = await priorityRulesModel.find({
                        tenant_id: tenant_id,
                        pay_type: 'BOTH',
                        rule_code: 'default_rule',
                    });
                }

            }
        }

        // })
    } else {
        var newData2 = []
        console.log("length----", priorityRules.length)
        var tenant_id = trim(req.body.tenant_id)
        if (req.body.pay_type) {
            var pay_type = trim(req.body.pay_type)
            console.log("if part--", tenant_id, pay_type)
            newData2 = await priorityRulesModel.find({
                tenant_id: tenant_id,
                pay_type: pay_type.toUpperCase(),
                rule_code: 'default_rule',
            });

        }
        console.log("newData2  --", newData2.length)
        if (newData2.length == 0) {
            console.log("else part--", tenant_id)
            newData2 = await priorityRulesModel.find({
                tenant_id: tenant_id,
                pay_type: 'BOTH',
                rule_code: 'default_rule',
            });
        }
    }
    console.log("newData2last---", newData2)
    return newData2

}

exports.recommendedPriorityRule = async (req, zone) => {
    var priorityRules = [];
    var priorityRules1 = [];
    console.log("zone@@---", zone)
    if (req.body.platform && (req.body.platform !== undefined || req.body.platform !== 'undefined' || req.body.platform !== '')
      && req.body.pay_type && (req.body.pay_type !== undefined || req.body.pay_type !== 'undefined' || req.body.pay_type !== '')
      && req.body.tenant_id && (req.body.tenant_id !== undefined || req.body.tenant_id !== 'undefined' || req.body.tenant_id !== '')) {
      if (zone && req.body.weight && (zone !== '' || zone !== undefined) && (req.body.weight !== undefined || req.body.weight !== 'undefined' || req.body.weight !== '')) {
        console.log("req.body---", req.body)
        var tenant_id = trim(req.body.tenant_id)
        var pay_type = trim(req.body.pay_type)
        var weight = trim(req.body.weight)
        var zone = trim(zone)
        var weight1 = parseFloat(weight)
        console.log("weight1---", weight1)
        if (pay_type) {
          priorityRules1 = await priorityRulesModel.find({
            tenant_id: tenant_id,
            pay_type: pay_type.toUpperCase(),
            zone: zone,
            rule_code: 'WZ',
            from: { "$lt": weight1 },
            to: { "$gte": weight1 },
          });
          console.log("priorityRules1.length---",priorityRules1.length)
          if (priorityRules1.length != 0) {
            for (let data of priorityRules1) {
              if (data.list != '') {
                priorityRules = priorityRules1
              } else {
                priorityRules = priorityRules
              }
            }
          } else {
            priorityRules = priorityRules
          }
  
        }
  
        if (priorityRules.length == 0) {
          priorityRules1 = await priorityRulesModel.find({
            tenant_id: tenant_id,
            pay_type: 'BOTH',
            rule_code: 'WZ',
            zone: zone,
            from: { "$lt": weight1 },
            to: { "$gte": weight1 },
          });
          if (priorityRules1.length != 0) {
            for (let data of priorityRules1) {
              if (data.list != '') {
                priorityRules = priorityRules1
              } else {
                priorityRules = priorityRules
              }
            }
          } else {
            priorityRules = priorityRules
          }
        }
      }
      if (priorityRules.length == 0) {
        if (zone && (zone !== '' || zone !== undefined) && (req.body.weight === undefined || req.body.weight === 'undefined' || req.body.weight === '' || req.body.weight)) {
          // console.log("zone rule")
          var tenant_id = trim(req.body.tenant_id)
          var pay_type = trim(req.body.pay_type)
          var zone = trim(zone)
          if (pay_type) {
            priorityRules1 = await priorityRulesModel.find({
              tenant_id: tenant_id,
              rule_code: 'ZM',
              pay_type: pay_type.toUpperCase(),
              zone: zone,
            });
            if (priorityRules1.length != 0) {
              for (let data of priorityRules1) {
                if (data.list != '') {
                  priorityRules = priorityRules1
                } else {
                  priorityRules = priorityRules
                }
              }
            } else {
              priorityRules = priorityRules
            }
          }
          if (priorityRules.length == 0) {
            priorityRules1 = await priorityRulesModel.find({
              tenant_id: tenant_id,
              rule_code: 'ZM',
              pay_type: 'BOTH',
              zone: zone,
            });
            if (priorityRules1.length != 0) {
              for (let data of priorityRules1) {
                if (data.list != '') {
                  priorityRules = priorityRules1
                } else {
                  priorityRules = priorityRules
                }
              }
            } else {
              priorityRules = priorityRules
            }
          }
  
        }
      }

      if (priorityRules.length == 0) {
        if (req.body.weight && (zone === '' || req.body.weight !== undefined || zone)) {
          // console.log("weight rule")
          var tenant_id = trim(req.body.tenant_id)
          var pay_type = trim(req.body.pay_type)
          var weight = trim(req.body.weight)
          var weight1 = parseFloat(weight)
          console.log("weight1---", weight1)
          console.log("weight0---", weight)
          if (pay_type) {
            priorityRules1 = await priorityRulesModel.find({
              tenant_id: tenant_id,
              rule_code: 'WM',
              pay_type: pay_type.toUpperCase(),
              from: { "$lt": weight1 },
              to: { "$gte": weight1 },
            });
            console.log("priorityRules1.length--WM --",priorityRules1.length)
            if (priorityRules1.length != 0) {
              for (let data of priorityRules1) {
                if (data.list != '') {
                  priorityRules = priorityRules1
                } else {
                  priorityRules = priorityRules
                }
              }
            } else {
              priorityRules = priorityRules
            }
          }
          if (priorityRules.length == 0) {
            priorityRules1 = await priorityRulesModel.find({
              tenant_id: tenant_id,
              rule_code: 'WM',
              pay_type: 'BOTH',
              from: { "$lt": weight1 },
              to: { "$gte": weight1 }
            });
            if (priorityRules1.length != 0) {
              for (let data of priorityRules1) {
                if (data.list != '') {
                  priorityRules = priorityRules1
                } else {
                  priorityRules = priorityRules
                }
              }
            } else {
              priorityRules = priorityRules
            }
          }
  
        }
  
      }
      
      if (priorityRules.length == 0) {
        if (pay_type) {
          priorityRules1 = await priorityRulesModel.find({
            tenant_id: tenant_id,
            pay_type: pay_type.toUpperCase(),
            rule_code: 'default_rule'
          });
          if (priorityRules1.length != 0) {
            for (let data of priorityRules1) {
              if (data.list != '') {
                priorityRules = priorityRules1
              } else {
                priorityRules = priorityRules
              }
            }
          } else {
            priorityRules = priorityRules
          }
        }
        
        if (priorityRules.length == 0) {
          console.log("priorityRules---",priorityRules.length)
          priorityRules1 = await priorityRulesModel.find({
            tenant_id: tenant_id,
            pay_type: 'BOTH',
            rule_code: 'default_rule'
          });
          console.log("priorityRules1---",priorityRules1)
          if (priorityRules1.length != 0) {
            for (let data of priorityRules1) {
              if (data.list != '') {
                priorityRules = priorityRules1
              } else {
                priorityRules = priorityRules
              }
            }
          } else {
            priorityRules = priorityRules
          }
        }
  
      }
      // console.log("priorityRules---",priorityRules)
    } else {
      // return res.status(400).json({
      priorityRules = 'All fields are required....'
      // })
    }
    return priorityRules
  
  }