const mysql = require('mysql2');
const dotenv = require('dotenv');
dotenv.config({path:__dirname+'/.env'});
const CLIENT_HOST = process.env.CLIENT_HOST
const CLIENT_PORT = process.env.CLIENT_PORT
const CLIENT_USER = process.env.CLIENT_USER
const CLIENT_PASSWORD = process.env.CLIENT_PASSWORD



exports.getMysqlDBClient = async(database, res, next) => {
    console.log("database---",database)
    const connection2 = mysql.createConnection({
        host: CLIENT_HOST, // host for connection
        port: CLIENT_PORT, // default port for mysql is 3306
        database: database,//'system', // database from which we want to connect out node application
        user: CLIENT_USER, // username of the mysql connection
        password: CLIENT_PASSWORD // password of the mysql connection
        });
      
       connection2.connect(function (err,result,next) {
          if(err){
              console.log("error occured while connecting");
          }
          else{
              console.log("connection created with Mysql successfully");
          }
       });
      return connection2
}