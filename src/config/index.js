const dbSystem = require("./db.system");
const Sequelize = require("sequelize");


const sequelizeTwo = new Sequelize(dbSystem.DB, dbSystem.USER, dbSystem.PASSWORD, {
  host: dbSystem.HOST,
  dialect: dbSystem.dialect,
  operatorsAliases: false,

  pool: {
    max: dbSystem.pool.max,
    min: dbSystem.pool.min,
    acquire: dbSystem.pool.acquire,
    idle: dbSystem.pool.idle
  }
});


const system = {};

system.Sequelize = Sequelize;
system.sequelize = sequelizeTwo;
system.ilogix_standard_pincodes_master = require("../models/ilogix_standard_pincodes_master")(sequelizeTwo, Sequelize);
system.consignor_standard_billing_zones = require("../models/consignor_standard_billing_zones")(sequelizeTwo, Sequelize);
system.ilogix_standard_other_pincodes = require("../models/ilogix_standard_other_pincodes")(sequelizeTwo, Sequelize);

module.exports = {system};
