const database = require('../config/index');
const sequelize = require('sequelize')
const system = database.system;

const searchData = async function (client_name) {
    var query = "select 'V1' as platform,h.fqdn as client_name,h.website_id as tenant_id,w.uuid as database_name from hostnames h left join websites w on h.website_id=w.id where h.fqdn like '" + client_name + "%' and h.fqdn like '%instashipin%' and h.under_maintenance_since is null";
    return system.sequelize.query(query, { type: system.sequelize.QueryTypes.SELECT });
}

const reverseCourier = async function () {
    var query = "select code from courier_vendors where rp = 1";
    return system.sequelize.query(query, { type: system.sequelize.QueryTypes.SELECT });
}

const getWebsiteData = async function (tenant_id) {
    console.log("tenant_id---",Number(tenant_id));
    var query = "select uuid from websites where id = " + Number(tenant_id);
    return system.sequelize.query(query, { type: system.sequelize.QueryTypes.SELECT });
}

const getServiceableCourier = async function (deliveryPincode, courier_code_list) {
    var query = 'select DISTINCT c.code as courier_id from serviceable_pincodes s left join courier_vendors c on s.courier_id=c.id where s.pincode="' + deliveryPincode + '" and s.lm_status="1" and c.code in (' + courier_code_list + ')';
    return system.sequelize.query(query, { type: system.sequelize.QueryTypes.SELECT });
}


module.exports ={
    searchData,
    reverseCourier,
    getWebsiteData,
    getServiceableCourier
}