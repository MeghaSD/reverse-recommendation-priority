const dotenv = require('dotenv');
const mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;  
var currentPath = process.cwd();
dotenv.config({path:currentPath +'/.env'});
const express = require("express");
const bodyParser = require('body-parser');
var path = require('path');
const app = express();
var destPath = __dirname + "\\public\\";
app.use(express.static(path.join(__dirname, 'public')));
const logger = require('./exception/logger');

app.use(bodyParser.json());
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

//parsing middlewares
app.use(bodyParser.json({limit:'50mb',extended:true}));
app.use(bodyParser.urlencoded({limit:'100mb',extended:true}));


var conn = mongoose.createConnection(process.env.MONGO_URI);
var conn2 = mongoose.createConnection(process.env.MONGO_URI2);


conn.on('connected', () => {
  console.log('connected to reverse_recommendation_priority');
});

conn2.on('connected', () => {
  console.log('connected to instashipin');
});

conn.on('disconnected', () => {
  console.log('connection disconnected reverse_recommendation_priority');
});

conn2.on('disconnected', () => {
  console.log('connection disconnected instashipin');
});

app.get('/', (req, res) => {
    res.send('Hello World!')
  });


function logErrors (err, req, res, next) {
  logger.error(err)
  next(err);
}

function errorHandler (err, req, res, next) {
  res.status(500).send({ error: 'Something went wrong.' })
}

app.get('/errorhandler', (req, res, next) => {
  try {
    throw new Error('Wowza!')
  } catch (error) {
    next(error)
  }
})

var priority_rules = require('./routes/routes.js');


// routes for common controllers
app.use(`/priority_rules`, priority_rules);
 
// error handler middelware
app.use(logErrors)
app.use(errorHandler)


  // server listening 
const port = process.env.API_PORT
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
module.exports = app;