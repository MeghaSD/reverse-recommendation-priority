const crypto = require('crypto');
const config = require('config');

const secret = '0d7c5c5f-768c-4d98-8900-13aadaa21937';
const ttl = 86400 * 1000; // 1 day, 
const algorithm = 'aes256';
const inputEncoding = 'utf8';
const outputEncoding = 'hex';

function genRandomString(length) {
    return crypto.randomBytes(Math.ceil(length / 2))
      .toString('hex')
      .slice(0, length);
  }

function sha512(password, salt) {
    const hash = crypto.createHmac('sha512', getStringValue(salt));
    hash.update(password);
    const passwordHash = hash.digest('hex');
  
    return {
      salt,
      passwordHash,
    };
  }

  function getStringValue(data) {
    if (typeof data === 'number' || data instanceof Number) {
      return data.toString();
    }
    if (!Buffer.isBuffer(data) && typeof data !== 'string') {
      throw new TypeError('Data for password or salt must be a string or a buffer');
    }
    return data;
  }

  function saltHashPassword(password) {
    const salt = genRandomString(16);
    return sha512(getStringValue(password), salt);
  }


  module.exports = {
    saltHashPassword,
    sha512,
  };