var mongoose = require('mongoose');
const dotenv = require('dotenv');
var currentPath = process.cwd();
dotenv.config({path:currentPath +'/.env'});
const database = process.env.MONGO_URI2;
var conn2 = mongoose.createConnection(database);
var websites2Schema = new mongoose.Schema({
    source_type: { type: String, required: true },
    user_type: { type: String, required: true },
    account_details: { type: Object, required: true},
    name: { type: String, required: true},
    enabled_couriers: { type: Array, required: true },  
    api_credentials: { type: Array, required: true},
    updated_at: { type: Date, required: true},
    created_at : { type: Date, required: true},
    erp_inserted: { type: Boolean, required: true},
    erp_validation_errors: { type: String, default: ''},
    client_flag: { type: Object, required: true},
    mainvendor: { type: String, required: true},
    rate_cards: { type: Array, required: true},
    termscondition: { type: Boolean, required: true},
    training: { type: Object, required: true},
    pickup_addresses: {type: Array, required: true},
    gst: { type: String, default: ''},
    dispatch_label_type: { type: Number, required: true},
    features: { type: Object, default: ''},
    is_surface: {type: String, default: ''},
    fulfillments: {type: Object, default: ''},
    reverse_enabled_couriers: { type: Array, required: true},
    couriers_history: {type: Array, required: true},
    reverse_couriers_history: {type: Array, required: true},
    slabs_activated: { type: Array, required: true}
},{
    versionKey: false // You should be aware of the outcome after set to false (_v column not generate)
}
);

module.exports = conn2.model('Websites', websites2Schema);