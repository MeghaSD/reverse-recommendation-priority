// const db2 = require('../../app/models/index2')
// const sequelize = db2.sequelize
module.exports = (sequelize, Sequelize) => {

    const ilogix_standard_pincodes_master = sequelize.define("ilogix_standard_pincodes_master", {

        pincode: {

            type: Sequelize.STRING

        },

        state: {

            type: Sequelize.STRING

        },
        city: {

            type: Sequelize.INTEGER

        },
        create_by: {
            type: Sequelize.INTEGER
        },
        created_datetime: {
            type: Sequelize.DATE
        },
        is_deleted: {
            type: Sequelize.TINYINT
        },
        is_custom: {
            type: Sequelize.STRING
        }


    });

    return ilogix_standard_pincodes_master;

};