var mongoose = require('mongoose');
const validator = require("validator");
const bcrypt = require("bcryptjs");
const dotenv = require('dotenv');
var currentPath = process.cwd();
dotenv.config({path:currentPath +'/.env'});
const database = process.env.MONGO_URI
var conn = mongoose.createConnection(database);
var WebsitesSchema = new mongoose.Schema({
    tenant_id: { type: String, required: true },
    userName: { type: String, required: true},
    email: {
        type: String,
        required: [true, "Please fill your email"], unique: true,
        lowercase: true,
        validate: [validator.isEmail, " Please provide a valid email"],
    },
    password: {
        type: String,
        required: [true, "Please fill your password"],
        minLength: 6,
        select: false,
      },
    passwordHash : {type:String, required : true},
    salt : {type:String, required: true},
},{
    versionKey: false // You should be aware of the outcome after set to false (_v column not generate)
}
);

// This is Instance Method that is gonna be available on all documents in a certain collection
// WebsitesSchema.methods.correctPassword = async function(
//     typedPassword,
//     originalPassword,
//   ) {
//     return await bcrypt.compare(typedPassword, originalPassword);
//   };
  // module.exports = mongoose.model('Websites',WebsitesSchema);
module.exports = conn.model('Websites', WebsitesSchema);