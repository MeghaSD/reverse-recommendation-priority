var mongoose = require('mongoose');
const dotenv = require('dotenv');
require('mongoose-double')(mongoose);
var currentPath = process.cwd();
dotenv.config({path:currentPath +'/.env'});
const database = process.env.MONGO_URI;
var conn = mongoose.createConnection(database);
var SchemaTypes = mongoose.Schema.Types;
var PriorityRulesSchema = new mongoose.Schema({
    tenant_id: { type: String, required: true },
    rule_code: { type: String, required: true },
    weight: { type: String, required: false },
    zone: { type: String, required: false },
    pay_type: { type: String, required: true,},
    list: { type: Array, required: true },
    from: { type: SchemaTypes.Double, required: false},
    to: { type: SchemaTypes.Double, required: false}   
},{
    versionKey: false // You should be aware of the outcome after set to false (_v column not generate)
}
);

// module.exports = mongoose.model('Priority_rules',PriorityRulesSchema);
module.exports = conn.model('Priority_rules', PriorityRulesSchema);
